<?php
ob_start();
session_start();
require_once("../class/Include.class.php");
require_once("../class/Url.php");
require_once("../class/Carrinho.class.php");
$obj_site = new Site();


// verifico se existe o login
if (!isset($_SESSION[funcionario])) {
  Util::script_location('./index.php');
}


$data = date("Ymd");

//$_SESSION[total_pedidos] = 0;




?>

<!DOCTYPE html>
<html>
	<!-- topo -->
  <?php require_once('../includes/head.php') ?>
  <?php require_once('../includes/js_css.php') ?>
  <!-- topo -->




<body>




    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center top30">
              <a href="<?php echo Util::caminho_projeto() ?>/" title="início">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="início" class="">
              </a>
            </div>
        </div>
    </div>



		<div class="container">
			<div class="row top100 bottom100">

        <div class="col-xs-12 lista-pedido">

          <h4>
            Pedidos da loja:
            <span> <?php Util::imprime( Util::troca_value_nome($_SESSION[funcionario][id_loja], 'tb_lojas', 'idloja', 'titulo') ); ?></span>
          </h4>

              <div class="pull-left">
                <h2 class="top10 bottom10">Listando os pedidos do dia <?php echo Util::formata_data($data); ?></h2>
              </div>

              <div class="pull-right">
                <p>A página será atualizada em <span id="counter"></span> segundo(s).</p>
              </div>

              <div class="clearfix"></div>


              <table class="table table-hover">
                <thead>
                  <tr>
                    <th class=text-nowrap scope=row>Data</th>
                    <td>Hora</td>
                    <td>Nome</td>
                    <td>Celular</td>
                    <td>Fixo</td>
                    <td class="text-center">Opção</td>
                  </tr>
                </thead>
                <tbody>
              <?php
              $id_loja = $_SESSION[funcionario][id_loja];
              //$result = $obj_site->select("tb_vendas", "and data = '$data' and id_loja = $id_loja order by data desc, hora desc ");
              //$result = $obj_site->select("tb_vendas", "and data = '$data' and id_loja = $id_loja order by data desc, hora desc ");
              $result = $obj_site->select("tb_vendas", "and data = '$data' order by data desc, hora desc ");
              if (mysql_num_rows($result) > 0) {

                //  verifico se chegou algum pedido novo
                if(mysql_num_rows($result) > $_SESSION[total_pedidos]){
                  echo '<audio autoplay id="audio-example">   <source src="alert.mp3"> </audio> ';
                  echo '
                      <script>
                      jQuery(document).ready(function () {
                        notifyMe("Um novo pedido foi efetuado.")
                      });
                      </script>
                      ';
                }




                //  armazeno a qtd
                $_SESSION[total_pedidos] = mysql_num_rows($result);


                while($row = mysql_fetch_array($result)){


                ?>
                    <tr class="<?php if($row[lido] == 'NAO'){ echo 'info'; } ?>">
                      <th class=text-nowrap scope=row><?php echo Util::formata_data($row[data]); ?></th>
                      <td><?php Util::imprime($row[hora], 5); ?></td>
                      <td><?php Util::imprime($row[nome]); ?></td>
                      <td><?php Util::imprime($row[celular]); ?></td>
                      <td><?php Util::imprime($row[fixo]); ?></td>
                      <td class="text-center">
                        <a target="_blank" href="<?php echo Util::caminho_projeto() ?>/imprime-pedido/?id=<?php echo $row[url_criptografada] ?>" title="Imprimir pedido" data-toggle="tooltip" data-placement="top" >
                          <i class="glyphicon glyphicon-print"></i>
                        </a>

                        <a class="left15" target="_blank" href="<?php echo Util::caminho_projeto() ?>/visualiza-pedido/?id=<?php echo $row[url_criptografada] ?>&action=lido" title="Visualizar pedido" data-toggle="tooltip" data-placement="top" >
                          <i class="glyphicon glyphicon-eye-open"></i>
                        </a>

                      </td>
                    </tr>
                <?php
                }
              }
              ?>
                </tbody>
              </table>







      <br>
      <h1>Pedidos do dia anterior</h1>


      <?php
      $dia_anterior = Util::formata_data( date('d/m/Y', strtotime("-1 days")) , 'banco');

      $result = $obj_site->select("tb_vendas", "and data = '$dia_anterior' and id_loja = $id_loja order by data desc, hora desc ");
      if (mysql_num_rows($result) == 0) {
          echo "<br><h2>Nenhum pedido encontrado.</h2>";
      }else{
        ?>

        <table class="table table-hover">
                <thead>
                  <tr>
                    <th class=text-nowrap scope=row>Data</th>
                    <td>Hora</td>
                    <td>Nome</td>
                    <td>Celular</td>
                    <td>Fixo</td>
                    <td class="text-center">Opção</td>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    while($row = mysql_fetch_array($result)){

                      ?>
                          <tr class="<?php if($row[lido] == 'NAO'){ echo 'info'; } ?>">
                            <th class=text-nowrap scope=row><?php echo Util::formata_data($row[data]); ?></th>
                            <td><?php Util::imprime($row[hora], 5); ?></td>
                            <td><?php Util::imprime($row[nome]); ?></td>
                            <td><?php Util::imprime($row[celular]); ?></td>
                            <td><?php Util::imprime($row[fixo]); ?></td>
                            <td class="text-center">
                              <a target="_blank" href="<?php echo Util::caminho_projeto() ?>/imprime-pedido/?id=<?php echo $row[url_criptografada] ?>" title="Imprimir pedido" data-toggle="tooltip" data-placement="top" >
                                <i class="glyphicon glyphicon-print"></i>
                              </a>

                              <a class="left15" target="_blank" href="<?php echo Util::caminho_projeto() ?>/visualiza-pedido/?id=<?php echo $row[url_criptografada] ?>&action=lido" title="Visualizar pedido" data-toggle="tooltip" data-placement="top" >
                                <i class="glyphicon glyphicon-eye-open"></i>
                              </a>

                            </td>
                          </tr>
                      <?php
                      }
                    }
                    ?>



                </tbody>
        </table>



        </div>


			</div>
		</div>










	<?php //require_once('../includes/rodape.php') ?>

</body>
</html>








<script type="text/javascript">
      var cntr = 60;
      function tick()
      {
          document.getElementById("counter").innerHTML = cntr--;

          if (cntr > 0)
          {
              setTimeout(tick, 1000);
          } else {
              window.location='<?php echo Util::caminho_projeto() ?>/pedidos-lojas/lista_pedidos.php';
          }
      }

          tick();

  </script>





<script type="text/javascript">
  function notifyMe(msg) {
    // Let's check if the browser supports notifications
    if (!("Notification" in window)) {
      alert("Esse navegado não suporta notificações.");
    }

    // Let's check if the user is okay to get some notification
    else if (Notification.permission === "granted") {
      // If it's okay let's create a notification
      var notification = new Notification(msg);
    }

    // Otherwise, we need to ask the user for permission
    // Note, Chrome does not implement the permission static property
    // So we have to check for NOT 'denied' instead of 'default'
    else if (Notification.permission !== 'denied') {
      Notification.requestPermission(function (permission) {

        // Whatever the user answers, we make sure we store the information
        if(!('permission' in Notification)) {
          Notification.permission = permission;
        }

        // If the user is okay, let's create a notification
        if (permission === "granted") {
          var notification = new Notification(msg);
        }
      });
    }

    // At last, if the user already denied any notification, and you
    // want to be respectful there is no need to bother him any more.
  }
</script>
