<?php
ob_start();
session_start();
require_once("../class/Include.class.php");
require_once("../class/Url.php");
require_once("../class/Carrinho.class.php");
$obj_site = new Site();

?>

<!DOCTYPE html>
<html>
	<!-- topo -->
  <?php require_once('../includes/head.php') ?>
  <?php require_once('../includes/js_css.php') ?>
  <!-- topo -->
<body>

	<?php
  if(isset($_POST[email])){
  	if( $obj_site->efetuar_login_funcionario($_POST[email], $_POST[senha]) == false ){
  		Util::alert_bootstrap("Usuário ou senha incorretos.");
  	}else{
  		Util::script_location('lista_pedidos.php');
  	}
  }
	?>


    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center top30">
              <a href="<?php echo Util::caminho_projeto() ?>/" title="início">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="início" class="">
              </a>
            </div>
        </div>
    </div>






		<div class="container">
			<div class="row top100 bottom100 col-xs-6 col-xs-offset-3">
				<form class="FormContato1 pt20 " id="FormContato" role="form" method="post">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Email</label>
				    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="">
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">Senha</label>
				    <input type="password" name="senha" class="form-control" id="exampleInputPassword1" placeholder="">
				  </div>

                  <div class="text-center">
                      <button type="submit" class="btn btn-default">Login</button>
                  </div>


				</form>
			</div>
		</div>




	<?php //require_once('../includes/rodape.php') ?>

</body>
</html>




<script>
  $(document).ready(function() {
    $('.FormContato1').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      senha: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>
