<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body>




  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->
  <div class="container-fluid relative">
    <div class="row">

      <div id="container_banner">

        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");

            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>

      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->



  <div class="topo-site">
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/topo.php') ?>
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
  </div>


  <!-- ======================================================================= -->
  <!-- CATEGORIAS HOME -->
  <!-- ======================================================================= -->
  <div class="container ">
    <div class="row lista_categorias">
      <div class="lato_black text-center">
        <div id="slider" class="flexslider">
          <ul class="slides">

            <?php
            $i=0;
            $result = $obj_site->select("tb_categorias_produtos");
            if (mysql_num_rows($result) > 0) {

              while ($row = mysql_fetch_array($result)) {
                ?>
                <li class="<?php if($i++ == 0){ echo 'active'; }?>">
                  <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" >
                    <h4><?php Util::imprime($row[titulo]); ?></h4>
                  </a>
                </li>
                <?php
              }

            }
            ?>

          </ul>
        </div>
      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- CATEGORIAS HOME -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- PRODUTOS    -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row">


      <?php
      $result = $obj_site->select("tb_produtos","and imagem <> '' ORDER BY RAND() LIMIT 9");
      if (mysql_num_rows($result) > 0) {
        $i = 0;
        while ($row = mysql_fetch_array($result)) {
          ?>

          <div class="col-xs-4 padding0 produtos_home">
            <div class="thumbnail">
              <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 379, 271, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
              </a>
              <div class="caption">
                <h1 class="text-uppercase"><?php Util::imprime($row[titulo]); ?></h1>
                <div class="top20">
                  <p>
                    <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="EFETUAR PEDIDO" class="btn btn_produtos_orcamento" role="button" >
                      <i class="fa fa-shopping-cart right15"></i>
                      EFETUAR PEDIDO
                    </a>
                    <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn_saiba_mais" role="button">SAIBA MAIS <i class="fa fa-plus-circle left15"></i></a>
                  </p>
                </div>
              </div>
            </div>
          </div>

          <?php
          if($i == 2){
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }

        }
      }
      ?>

      <div class="col-xs-12 text-right">
        <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn_todos_prod" role="button">ABRIR TODOS OS PRODUTOS </a>
      </div>


    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- PRODUTOS    -->
  <!-- ======================================================================= -->

  <div class="top50">  </div>
  <!-- ======================================================================= -->
  <!-- onde estamos    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/onde.php') ?>
  <!-- ======================================================================= -->
  <!-- onde estamos    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- CARDAPIO    -->
  <!-- ======================================================================= -->
  <div class="container top50">
    <div class="row cardapio_home">

      <ul >
        <?php
        $i=0;
        $result = $obj_site->select("tb_servicos","and imagem <> '' order by rand() limit 4");
        if (mysql_num_rows($result) > 0) {

          while ($row = mysql_fetch_array($result)) {
            ?>

        <li class="col-xs-3 padding0">
          <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 300, 501, array("class"=>"input100 img", "alt"=>"$row[titulo]")) ?>
          </a>
        </li>
        <?php
      }

      }
      ?>

      </ul>






    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- CARDAPIO    -->
  <!-- ======================================================================= -->

<?php /*
  <!-- ======================================================================= -->
  <!-- novidades sobre tv    -->
  <!-- ======================================================================= -->
  <div class="container top50">
    <div class="row bg_tv">

      <div class="col-xs-6 col-xs-offset-1 top265">
        <div class="padding0">
        <a href="<?php echo Util::caminho_projeto() ?>/anuncie" class="btn btn_saiba_tv left25">SAIBA MAIS</a>
        </div>
      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- novidades sobre tv    -->
  <!-- ======================================================================= -->
*/ ?>


  <!-- ======================================================================= -->
  <!--EMPRESA HOME   -->
  <!-- ======================================================================= -->
  <div class="container top50">
    <div class="row empresa_home">

      <div class="col-xs-12 text-center">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
        <div class="">
          <h1><?php Util::imprime($row[titulo]); ?></h1>
          <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_empresa_home.jpg" alt="" >
        </div>
        <div class="top45 bottom45">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>

      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!--EMPRESA HOME   -->
  <!-- ======================================================================= -->





  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">



<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',
    autoScaleSlider: false,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      pauseOnHover: true
    }

  });
});
</script>



<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    itemWidth: 100,
    itemMargin: 1,
    inItems: 1,
    maxItems: 6
  });
});
</script>
