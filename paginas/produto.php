<?php
$obj_data = new Data_Hora();

// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",3) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  124px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!--PRODUTO DENTRO-->
  <!-- ======================================================================= -->
  <div class='container'>
    <div class="row top235">


      <!-- ======================================================================= -->
      <!-- SLIDER CATEGORIA -->
      <!-- ======================================================================= -->
      <div class="col-xs-5 slider_prod_geral">

        <?php
        $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
        if(mysql_num_rows($result) == 0){
          echo "<div class='imagem_default'><img src='../imgs/default.png' alt=''></div>";
        }else{
          ?>


        <!-- Place somewhere in the <body> of your page -->
        <div id="slider" class="flexslider">
          <ul class="slides slider-prod">
            <?php
            $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
            if(mysql_num_rows($result) > 0)
            {
              while($row = mysql_fetch_array($result)){
                //$result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
                //$row_categoria = mysql_fetch_array($result_categoria);
                ?>
                <li class="zoom">
                  <a href="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $row[imagem]; ?>" title="<?php Util::imprime($row[legenda]); ?>"  class="group4">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 500, 406, array("class"=>"", "alt"=>"$row[legenda]")) ?>
                    <?php if (!empty($row[legenda])): ?>
                        <p class="flex-caption"><?php echo Util::imprime($row[legenda]) ?></p>
                    <?php endif; ?>
                  </a>
                </li>
                <?php
              }
            }


            ?>

            <!-- items mirrored twice, total of 12 -->
          </ul>
          <span>clique na imagem para Ampliar</span>

        </div>

      <?php } ?>


      </div>
      <!-- ======================================================================= -->
      <!-- SLIDER CATEGORIA -->
      <!-- ======================================================================= -->

      <div class="col-xs-7 empresa_titulo">
        <div class="bottom25">
          <h2><span><?php Util::imprime($dados_dentro[titulo]); ?></span></h2>
        </div>

        <div class="padding0 col-xs-12">
          <div class="dropdown">
            <a class=" btn_produto" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <img  src="<?php echo Util::caminho_projeto(); ?>/imgs/atentimento.png" alt="" />
            </a>
            <div class="dropdown-menu drop_prod pb40 pull-right" aria-labelledby="dropdownMenu1">


              <!-- ======================================================================= -->
              <!-- nossas lojas  -->
              <!-- ======================================================================= -->
              <?php
              $result = $obj_site->select("tb_lojas");
              if(mysql_num_rows($result) > 0){
                while($row = mysql_fetch_array($result)){
                  $i=0;

                  //  verifica se a loja está aberta
                  $lojas = $obj_site->verifica_loja_aberta($row[idloja]);
                  ?>

                  <div class="col-xs-3 top145">
                    <div class="fundo_lojas">




                      <div class="text-center pt15">
                        <h2><?php Util::imprime($row[titulo]); ?></h2>
                      </div>

                      <div class="clearfix"></div>


                      <!-- ======================================================================= -->
                      <!-- telefones  -->
                      <!-- ======================================================================= -->
                      <div class="media top35">
                        <div class="media-left media-middle">
                          <img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_telefone_topo.png" alt="">
                        </div>
                        <div class="media-body">
                          <h3 class="media-heading">
                            <?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?>

                            <?php if (!empty($row1[telefone2])): ?>
                              <?php Util::imprime($row[ddd2]); ?> <?php Util::imprime($row[telefone2]); ?>
                            <?php endif; ?>

                          </h3>
                        </div>
                      </div>

                      <?php if (!empty($row[telefone2])): ?>
        								<div class="media">
        									<div class="media-left media-middle">
        										<i class="fa fa-whatsapp"></i>
        									</div>
        									<div class="media-body">
        										<h3 class="media-heading">
        											<?php Util::imprime($row[ddd2]); ?> <?php Util::imprime($row[telefone2]); ?>
        										</h3>
        									</div>
        								</div>
        							<?php endif; ?>
                      <!-- ======================================================================= -->
                      <!-- telefones  -->
                      <!-- ======================================================================= -->

                      <!-- ======================================================================= -->
                      <!-- endereco  -->
                      <!-- ======================================================================= -->
                      <div class="media top20">
                        <div class="media-left media-middle">
                          <img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_map_topo.png" alt="">
                        </div>
                        <div class="media-body">
                          <h3 class="media-heading">
                            <span><?php Util::imprime($row[endereco]); ?>
                            </span>
                          </h3>
                        </div>
                      </div>
                      <!-- ======================================================================= -->
                      <!-- endereco  -->
                      <!-- ======================================================================= -->

                      <!-- ======================================================================= -->
                      <!-- entrega  -->
                      <!-- ======================================================================= -->
                      <div class="media horario top20">
                        <div class="media-left media-middle">
                          <img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_agenda_topo.png" alt="">
                        </div>
                        <div class="media-body">
                          <h3 class="media-heading">Horário de entrega
                            <span>: <br> <?php Util::imprime($lojas[horario_abertura], 5); ?> às <?php Util::imprime($lojas[horario_fechamento], 5); ?>
                            </span>
                          </h3>
                        </div>
                      </div>
                      <!-- ======================================================================= -->
                      <!-- entrega  -->
                      <!-- ======================================================================= -->

                      <div class="padding0">
                        <a href="<?php Util::imprime($row[link_maps]); ?>" target="_blank" class="btn btn_localizacao input100">ABRIR NO MAPS</a>
                      </div>
                      <div class="padding0 top5">
                        <a class="btn btn_localizacao input100" href="<?php echo Util::caminho_projeto() ?>/loja/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                          SAIBA MAIS
                        </a>
                      </div>


                    </div>
                  </div>





                  <?php
                  if ($i==3) {
                    echo "<div class='clearfix'>  </div>";
                  }else {
                    $i++;
                  }
                }
              }
              ?>

              <!-- ======================================================================= -->
              <!-- nossas lojas  -->
              <!-- ======================================================================= -->



            </div>
          </div>

          <!-- ======================================================================= -->
          <!-- nossas lojas  -->
          <!-- ======================================================================= -->
        </div>






        <div class="col-xs-12 top40">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 6);?>
          <div class="top60 ">
            <?php //require_once('./includes/opcoes_produtos.php'); ?>
            


            <div class="top25 text-center">
                <h1>CONTATE A LOJA MAIS PERTO DE VOCÊ</h1>
                <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_empresa_home.jpg" alt="">
            </div>


            <div class="fale top20">
                 <!-- ======================================================================= -->
                  <!-- nossas lojas  -->
                  <!-- ======================================================================= -->
                  <?php
                  $result = $obj_site->select("tb_lojas");
                  if(mysql_num_rows($result) > 0){
                    while($row = mysql_fetch_array($result)){
                      $i=0;
                      //  verifica se a loja está aberta
                      $lojas = $obj_site->verifica_loja_aberta($row[idloja]);
                      ?>

                      <div class="col-xs-12 padding0">


                        <table class="table">
                          <tr>
                            <td class="col-xs-3 padding0"><h2><?php Util::imprime($row[titulo]); ?></h2></td>

                            <td>
                              <!-- ======================================================================= -->
                              <!-- telefones  -->
                              <!-- ======================================================================= -->
                              <div class=" media">
                                <div class="media-left media-middle">
                                  <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_telefone_fale.png" alt="">
                                </div>
                                <div class="media-body">
                                  <h3 class="media-heading">
                                    <?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?>

                                    <?php if (!empty($row1[telefone2])): ?>
                                      <?php Util::imprime($row[ddd2]); ?> <?php Util::imprime($row[telefone2]); ?>
                                    <?php endif; ?>

                                  </h3>
                                </div>
                              </div>
                              <!-- ======================================================================= -->
                              <!-- telefones  -->
                              <!-- ======================================================================= -->
                            </td>

                            <td>
                              <!-- ======================================================================= -->
                              <!-- entrega  -->
                              <!-- ======================================================================= -->
                              <div class="padding0 media horario">
                                <div class="media-left media-middle">
                                  <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_agenda.png" alt="">
                                </div>
                                <div class="media-body">
                                  <h3 class="media-heading">Horário de entrega <span>: <br> <?php Util::imprime($lojas[horario_abertura], 5); ?> às <?php Util::imprime($lojas[horario_fechamento], 5); ?></span>
                                  </h3>
                                </div>
                              </div>
                              <!-- ======================================================================= -->
                              <!-- entrega  -->
                              <!-- ======================================================================= -->
                            </td>
                          </tr>
                        </tbody>

                      </table>

                    </div>

                    <?php
                    if ($i==0) {
                      echo "<div class='clearfix'>  </div>";
                    }else {
                      $i++;
                    }
                  }
                }
                ?>

                <!-- ======================================================================= -->
                <!-- nossas lojas  -->
                <!-- ======================================================================= -->

            </div>


          </div>
        </div>




      </div>

      
      <div class="col-xs-12 empresa">
        <div class="top90 text-center">
          <h1>DETALHES DO PRODUTO</h1>
          <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_empresa_home.jpg" alt="">
        </div>

        <div class="top25">
          <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
        </div>
      </div>
      

    </div>
  </div>
  <!-- ======================================================================= -->
  <!--PRODUTO DENTRO-->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- VEJA TAMBEM    -->
  <!-- ======================================================================= -->
  <div class="container top20">
    <div class="row">
              
      <div class="col-xs-12 bottom30">
        <h1>VEJA TAMBÉM</h1>
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_empresa_home.jpg" alt="">
      </div>

      <?php
      $i = 0;
      $result = $obj_site->select("tb_produtos", "order by rand() limit 3");
      if(mysql_num_rows($result) == 0){
        echo "<h2 class='bg-info top25' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
      }else{
        while ($row = mysql_fetch_array($result))
        {
          $result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
          $row_categoria = mysql_fetch_array($result_categoria);
          ?>

          <div class="col-xs-4 padding0 produtos_home">
            <div class="thumbnail">
              <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 379, 271, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
              </a>
              <div class="caption">
                <h1><?php Util::imprime($row[titulo]); ?></h1>
                <div class="top20">
                  <p>

                    <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="EFETUAR PEDIDO" class="btn btn_produtos_orcamento" role="button" >
                      <i class="fa fa-shopping-cart right15"></i>
                      EFETUAR PEDIDO
                    </a>
                    <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn_saiba_mais" role="button">SAIBA MAIS <i class="fa fa-plus-circle left15"></i></a>
                  </p>
                </div>
              </div>
            </div>
          </div>

          <?php
          if($i == 2){
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }

        }
      }

      ?>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- VEJA TAMBEM    -->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<script>
$(window).load(function() {
  // The slider being synced must be initialized first

  $('#slider').flexslider({
    animation: "slide",
    controlNav: true,
    animationLoop: false,
    slideshow: true,
    sync: "#carousel"
  });
});


</script>





<script>
$(document).ready(function() {
  $('.FormPedido').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {


      preco: {
        validators: {
          notEmpty: {
            message: 'Por favor, selecione uma opção.'
          }
        }
      },

      <?php if($dados_dentro[obrigatorio_quantidade] == 'SIM'): ?>
      quantidade: {
        validators: {
          notEmpty: {
            message: 'Por favor, selecione a quantidade desejada.'
          }
        }
      },
    <?php endif; ?>



      <?php echo $validacao_javascript; ?>

    }
  });
});
</script>
