<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",2) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  3px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top30 titulo_emp">
      <div class="col-xs-12 text-center">
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_personalizada.png" alt="">
        <div class="">
          <h2>CONHEÇA NOSSOS</h2>
          <h1 class="text-capitalize">Produtos</h1>
        </div>
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_personalizada.png" alt="">
      </div>

    </div>
  </div>
</div>
</div>
<!-- ======================================================================= -->
<!--  titulo -->
<!-- ======================================================================= -->






<!-- ======================================================================= -->
<!-- CATEGORIAS HOME -->
<!-- ======================================================================= -->
<div class="container top90">
  <div class="row lista_categorias">
    <div class="lato_black text-center">
      <div id="slider" class="flexslider">
        <ul class="slides">

          <?php
          $i=0;
          $result = $obj_site->select("tb_categorias_produtos");
          if (mysql_num_rows($result) > 0) {

            while ($row = mysql_fetch_array($result)) {
              ?>
              <?php $url1 = Url::getURL(1); ?>
              <li class="<?php if($url1 == "$row[url_amigavel]"){ echo 'active'; } ?>" >
                <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                  <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" >
                  <h4><?php Util::imprime($row[titulo]); ?></h4>
                </a>
              </li>
              <?php
            }

          }
          ?>

        </ul>
      </div>
    </div>

  </div>
</div>
<!-- ======================================================================= -->
<!-- CATEGORIAS HOME -->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- PRODUTOS    -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row">


    <?php


    //  busca os produtos sem filtro
    $result = $obj_site->select("tb_produtos", $complemento);




    $url1 = Url::getURL(1);

    //  FILTRA AS CATEGORIAS
    if (isset( $url1 )) {
      $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);
      $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
    }

    $result = $obj_site->select("tb_produtos", $complemento);

    ?>

    <?php
    if(mysql_num_rows($result) == 0){
      echo "<p class='bg-danger clearfix' style='padding: 20px;'>Nenhum produto encontrado</p>";
    }else{
      ?>
      <div class="col-xs-12 total-resultado-busca bottom10">
        <h1><span><?php echo mysql_num_rows($result) ?></span> PRODUTO(S) ENCONTRADO(S) . </h1>
      </div>

      <?php
      $i = 0;
      while($row = mysql_fetch_array($result))
      {
        $result1 = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
        $row1 = mysql_fetch_array($result1);
        ?>

        <div class="col-xs-4 padding0 produtos_home">
          <div class="thumbnail">
            <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 379, 271, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
            </a>
            <div class="caption">
              <h1 class="text-uppercase"><?php Util::imprime($row[titulo]); ?></h1>
              <div class="top20">
                <p>

                  <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="EFETUAR PEDIDO" class="btn btn_produtos_orcamento" role="button" >
                    <i class="fa fa-shopping-cart right15"></i>
                    EFETUAR PEDIDO
                  </a>
                  <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn_saiba_mais" role="button">SAIBA MAIS <i class="fa fa-plus-circle left15"></i></a>
                </p>
              </div>
            </div>
          </div>
        </div>

        <?php
        if($i == 2){
          echo '<div class="clearfix"></div>';
          $i = 0;
        }else{
          $i++;
        }

      }
    }
    ?>

  </div>
</div>
<!-- ======================================================================= -->
<!-- PRODUTOS    -->
<!-- ======================================================================= -->

<div class="top50">  </div>




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    itemWidth: 100,
    itemMargin: 1,
    inItems: 1,
    maxItems: 6
  });
});
</script>
