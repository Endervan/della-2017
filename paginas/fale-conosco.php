<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",7) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 3px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->





  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row  top80 titulo_emp">
      <div class="col-xs-12 text-center">
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_personalizada.png" alt="">
        <div class="top10">
          <h2>ENTRE EM CONTATO</h2>
          <h1 class="text-capitalize">Fale Conosco</h1>
        </div>
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_personalizada.png" alt="">
      </div>

    </div>
  </div>
  </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- fale conosco -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">

      <!--  ==============================================================  -->
      <!-- FORMULARIO CONTATOS-->
      <!--  ==============================================================  -->
      <div class="col-xs-8 padding0 top145 fale">

        <!-- ======================================================================= -->
        <!-- nossas lojas  -->
        <!-- ======================================================================= -->
        <?php
        $result = $obj_site->select("tb_lojas");
        if(mysql_num_rows($result) > 0){
          while($row = mysql_fetch_array($result)){
            $i=0;
            //  verifica se a loja está aberta
            $lojas = $obj_site->verifica_loja_aberta($row[idloja]);
            ?>

            <div class="col-xs-12 padding0">


              <table class="table">
                <tr>
                  <td class="col-xs-3 padding0"><h2><?php Util::imprime($row[titulo]); ?></h2></td>

                  <td>
                    <!-- ======================================================================= -->
                    <!-- telefones  -->
                    <!-- ======================================================================= -->
                    <div class=" media">
                      <div class="media-left media-middle">
                        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_telefone_fale.png" alt="">
                      </div>
                      <div class="media-body">
                        <h3 class="media-heading">
                          <?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?>

                          <?php if (!empty($row1[telefone2])): ?>
                            <?php Util::imprime($row[ddd2]); ?> <?php Util::imprime($row[telefone2]); ?>
                          <?php endif; ?>

                        </h3>
                      </div>
                    </div>
                    <!-- ======================================================================= -->
                    <!-- telefones  -->
                    <!-- ======================================================================= -->
                  </td>

                  <td>
                    <!-- ======================================================================= -->
                    <!-- entrega  -->
                    <!-- ======================================================================= -->
                    <div class="padding0 media horario">
                      <div class="media-left media-middle">
                        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_agenda.png" alt="">
                      </div>
                      <div class="media-body">
                        <h3 class="media-heading">Horário de entrega <span>: <br> <?php Util::imprime($lojas[horario_abertura], 5); ?> às <?php Util::imprime($lojas[horario_fechamento], 5); ?></span>
                        </h3>
                      </div>
                    </div>
                    <!-- ======================================================================= -->
                    <!-- entrega  -->
                    <!-- ======================================================================= -->
                  </td>
                </tr>
              </tbody>

            </table>

          </div>

          <?php
          if ($i==0) {
            echo "<div class='clearfix'>  </div>";
          }else {
            $i++;
          }
        }
      }
      ?>

      <!-- ======================================================================= -->
      <!-- nossas lojas  -->
      <!-- ======================================================================= -->


      <form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
        <div class="fundo_formulario">
          <div class="top40">
            <h3 class="left15">ENVIE UM E-MAIL</h3>
          </div>

          <!-- formulario orcamento -->

          <div class="col-xs-12 select_pick">
            <select name="select_categorias" class="selectpicker envia_select" data-max-options="1" data-live-search="true" title="SELECIONE A UNIDADE DESEJADA" data-width="100%">
              <optgroup label="SELECIONE">
                <?php
                $result = $obj_site->select("tb_lojas");
                if (mysql_num_rows($result) > 0) {
                  while($row = mysql_fetch_array($result)){
                    ?>
                    <option value="<?php Util::imprime($row[titulo]); ?>" data-tokens="<?php Util::imprime($row[titulo]); ?>">
                      <?php Util::imprime($row[titulo]); ?>
                    </option>
                    <?php
                  }
                }
                ?>
              </optgroup>
            </select>
          </div>

          <div class="clearfix">  </div>

          <div class="top20">
            <div class="col-xs-6">
              <div class="form-group input100 has-feedback">
                <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                <span class="fa fa-user form-control-feedback top15"></span>
              </div>
            </div>

            <div class="col-xs-6">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                <span class="fa fa-envelope form-control-feedback top15"></span>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>


          <div class="top20">
            <div class="col-xs-6">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                <span class="fa fa-phone form-control-feedback top15"></span>
              </div>
            </div>

            <div class="col-xs-6">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="assunto" class="form-control fundo-form1 input-lg input100" placeholder="ASSUNTO">
                <span class="glyphicon glyphicon-star form-control-feedback top5"></span>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>


          <div class="top15">
            <div class="col-xs-12">
              <div class="form-group input100 has-feedback">
                <textarea name="mensagem" cols="25" rows="10" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                <span class="fa fa-pencil form-control-feedback top15"></span>
              </div>
            </div>
          </div>

          <div class="col-xs-6 top30">

          </div>
          <div class="col-xs-6 text-right">
            <div class="top15 bottom25">
              <button type="submit" class="btn btn_formulario" name="btn_contato">
                ENVIAR
              </button>
            </div>
          </div>

        </div>
      </form>
    </div>
    <!--  ==============================================================  -->
    <!-- FORMULARIO CONTATOS-->
    <!--  ==============================================================  -->


    <!-- ======================================================================= -->
    <!-- CONTATOS  -->
    <!-- ======================================================================= -->
    <div class="col-xs-4">

      <div class="col-xs-12 text-center">
        <img class="input100" src="<?php echo Util::caminho_projeto(); ?>/imgs/empregada.png" alt="" >

      </div>

      <div class="telefone_contatos left20">

        <!-- Nav tabs -->
        <div class="menu_contatos">
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">

              <a>
                <div class="media">
                  <div class="media-left media-middle">
                    <img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_contato.png" alt="">
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">TIRE SUAS DUVIDAS <span class="clearfix">FALE CONOSCO</span></h4>
                  </div>
                </div>
              </a>
            </li>

            <li role="presentation" >
              <a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">

                <div class="media">
                  <div class="media-left media-middle">
                    <img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_trabalhe.png" alt="">
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">  FAÇA PARTE DA NOSSA EQUIPE <span class="clearfix">TRABALHE CONOSCO</span></h4>
                  </div>
                </div>

              </a>
            </li>
          </ul>

        </div>

      </div>
      <!-- ======================================================================= -->
      <!-- CONTATOS  -->
      <!-- ======================================================================= -->

    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- fale conosco -->
<!-- ======================================================================= -->





<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>




<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{
  echo $texto_mensagem = "
  Unidade Desejada : ".($_POST[select_categorias])." <br />
  Nome: ".($_POST[nome])." <br />
  Email: ".($_POST[email])." <br />
  Telefone: ".($_POST[telefone])." <br />
  Assunto: ".($_POST[assunto])." <br />

  Mensagem: <br />
  ".(nl2br($_POST[mensagem]))."
  ";

  ;




  Util::envia_email($config[email_fale_conosco], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
//  Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);

  Util::alert_bootstrap("Obrigado por entrar em contato.");
  unset($_POST);
}


?>



<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Insira seu nome.'
          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {
            message: 'Insira sua Mensagem.'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'Informe um email.'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Por favor informe seu numero!.'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
