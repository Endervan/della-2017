<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",11) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row  top80 titulo_emp">
      <div class="col-xs-12 text-center">
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_personalizada.png" alt="">
        <div class="top10">
          <h2>ENTRE EM CONTATO</h2>
          <h1 class="text-capitalize">Trabalhe Conosco</h1>
        </div>
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_personalizada.png" alt="">
      </div>

    </div>
  </div>
</div>
</div>
<!-- ======================================================================= -->
<!--  titulo -->
<!-- ======================================================================= -->





<div class="container">
  <div class="row">


    <div class="col-xs-8 padding0 fale top145">

        <!-- ======================================================================= -->
        <!-- nossas lojas  -->
        <!-- ======================================================================= -->
        <?php
        $result = $obj_site->select("tb_lojas");
        if(mysql_num_rows($result) > 0){
          while($row = mysql_fetch_array($result)){
            $i=0;
            //  verifica se a loja está aberta
            $lojas = $obj_site->verifica_loja_aberta($row[idloja]);
            ?>

            <div class="col-xs-12 padding0">


              <table class="table">
                <tr>
                  <td class="col-xs-3 padding0"><h2><?php Util::imprime($row[titulo]); ?></h2></td>

                  <td>
                    <!-- ======================================================================= -->
                    <!-- telefones  -->
                    <!-- ======================================================================= -->
                    <div class=" media">
                      <div class="media-left media-middle">
                        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_telefone_fale.png" alt="">
                      </div>
                      <div class="media-body">
                        <h3 class="media-heading">
                          <?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?>

                          <?php if (!empty($row1[telefone2])): ?>
                            <?php Util::imprime($row[ddd2]); ?> <?php Util::imprime($row[telefone2]); ?>
                          <?php endif; ?>

                        </h3>
                      </div>
                    </div>
                    <!-- ======================================================================= -->
                    <!-- telefones  -->
                    <!-- ======================================================================= -->
                  </td>

                  <td>
                    <!-- ======================================================================= -->
                    <!-- entrega  -->
                    <!-- ======================================================================= -->
                    <div class="padding0 media horario">
                      <div class="media-left media-middle">
                        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_agenda.png" alt="">
                      </div>
                      <div class="media-body">
                        <h3 class="media-heading">Horário de entrega <span>: <br> <?php Util::imprime($lojas[horario_abertura], 5); ?> às <?php Util::imprime($lojas[horario_fechamento], 5); ?></span>
                        </h3>
                      </div>
                    </div>
                    <!-- ======================================================================= -->
                    <!-- entrega  -->
                    <!-- ======================================================================= -->
                  </td>
                </tr>
              </tbody>

            </table>

          </div>

          <?php
          if ($i==0) {
            echo "<div class='clearfix'>  </div>";
          }else {
            $i++;
          }
        }
      }
      ?>

      <!-- ======================================================================= -->
      <!-- nossas lojas  -->
      <!-- ======================================================================= -->

    <!--  ==============================================================  -->
    <!-- FORMULARIO-->
    <!--  ==============================================================  -->
    <div class="col-xs-12 padding0 top50">
      <form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
        <div class="fundo_formulario">
          <div class="top40">
            <h3 class="left15">ENVIE SEUS DADOS</h3>
          </div>
          <!-- formulario orcamento -->
          <div class="top20">
            <div class="col-xs-6">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                <span class="fa fa-user form-control-feedback top15"></span>
              </div>
            </div>

            <div class="col-xs-6">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="email" class="form-control fundo-form1 input100 input-lg" placeholder="E-MAIL">
                <span class="fa fa-envelope form-control-feedback top15"></span>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>


          <div class="top20">
            <div class="col-xs-6">
              <div class="form-group input100 has-feedback ">
                <input type="text" name="telefone" class="form-control fundo-form1 input100  input-lg" placeholder="TELEFONE">
                <span class="fa fa-phone form-control-feedback top15"></span>
              </div>
            </div>

            <div class="col-xs-6">
              <div class="form-group input100 has-feedback ">
                <input type="text" name="escolaridade" class="form-control fundo-form1 input100  input-lg" placeholder="ESCOLARIDADE">
                <span class="fa fa-book form-control-feedback"></span>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="top20">
            <div class="col-xs-6">
              <div class="form-group input100 has-feedback">
                <input type="text" name="cargo" class="form-control fundo-form1 input100  input-lg" placeholder="CARGO" >
                <span class="fa fa-briefcase form-control-feedback"></span>
              </div>
            </div>

            <div class="col-xs-6">
              <div class="form-group input100 has-feedback ">
                <input type="text" name="area" class="form-control fundo-form1 input100  input-lg" placeholder="AREA">
                <span class="fa fa-building form-control-feedback"></span>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="top20">

            <div class="col-xs-6">
              <div class="form-group input100 has-feedback ">
                <input type="text" name="cidade" class="form-control fundo-form1 input100  input-lg" placeholder="CIDADE">
                <span class="fa fa-home form-control-feedback top15"></span>
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group input100 has-feedback ">
                <input type="text" name="estado" class="form-control fundo-form1 input100  input-lg" placeholder="ESTADO">
                <span class="fa fa-globe form-control-feedback top15"></span>
              </div>
            </div>

          </div>


          <div class="clearfix"></div>

          <div class="top20">
            <div class="col-xs-12">
              <div class="form-group input100 has-feedback ">
                <input type="file" name="curriculo" class="form-control fundo-form1 input100  input-lg" placeholder="CURRÍCULO">
                <span class="fa fa-file form-control-feedback top15"></span>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>


          <div class="top20">
            <div class="col-xs-12">
              <div class="form-group input100 has-feedback">
                <textarea name="mensagem" cols="30" rows="12" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                <span class="fa fa-pencil form-control-feedback top15"></span>
              </div>
            </div>
          </div>



          <div class="col-xs-12 text-right">
            <div class="top15 bottom25">
              <button type="submit" class="btn btn_formulario" name="btn_contato">
                ENVIAR
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <!--  ==============================================================  -->
    <!-- FORMULARIO-->
    <!--  ==============================================================  -->
  </div>

  <!-- ======================================================================= -->
  <!-- CONTATOS  -->
  <!-- ======================================================================= -->
  <div class="col-xs-4">

    <div class="col-xs-12 text-center">
      <img class="input100" src="<?php echo Util::caminho_projeto(); ?>/imgs/empregada.png" alt="" >

    </div>

    <div class="telefone_contatos left20">

      <!-- Nav tabs -->
      <div class="menu_contatos">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" >

            <a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">
              <div class="media">
                <div class="media-left media-middle">
                  <img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_contato.png" alt="">
                </div>
                <div class="media-body">
                  <h4 class="media-heading">TIRE SUAS DUVIDAS <span class="clearfix">FALE CONOSCO</span></h4>
                </div>
              </div>
            </a>
          </li>

          <li role="presentation" class="active" >
            <a >

              <div class="media">
                <div class="media-left media-middle">
                  <img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_trabalhe.png" alt="">
                </div>
                <div class="media-body">
                  <h4 class="media-heading">  FAÇA PARTE DA NOSSA EQUIPE <span class="clearfix">TRABALHE CONOSCO</span></h4>
                </div>
              </div>

            </a>
          </li>
        </ul>

      </div>

    </div>
    <!-- ======================================================================= -->
    <!-- CONTATOS  -->
    <!-- ======================================================================= -->

  </div>
</div>
</div>
<!-- ======================================================================= -->
<!-- fale conosco -->
<!-- ======================================================================= -->





<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>




<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{

  if(!empty($_FILES[curriculo][name])):
    $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
    $texto = "Anexo: ";
    $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
    $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
  endif;

  $texto_mensagem = "
  Nome: ".$_POST[nome]." <br />
  Email: ".$_POST[email]." <br />
  Telefone: ".$_POST[telefone]." <br />
  Escolaridade: ".$_POST[escolaridade]." <br />
  Cargo: ".$_POST[cargo]." <br />
  Area : ".$_POST[area]." <br />
  Cidade: ".$_POST[cidade]." <br />
  Estado: ".$_POST[estado]." <br />

  Mensagem: <br />
  ".nl2br($_POST[mensagem])."

  <br><br>
  $texto
  ";


  Util::envia_email($config[email_trabalhe_conosco], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
  Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
  Util::alert_bootstrap("Obrigado por entrar em contato.");
  unset($_POST);
}

?>




<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Informe nome.'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'Seu email.'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione seu numero.'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione sua Cidade.'
          }
        }
      },
      estado: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione seu Estado.'
          }
        }
      },
      escolaridade1: {
        validators: {
          notEmpty: {
            message: 'Sua Informação Acadêmica.'
          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
            maxSize: 5*1024*1024,   // 5 MB
            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {
            message: 'Insira sua Mensagem.'
          }
        }
      }
    }
  });
});
</script>


<!-- ======================================================================= -->
<!-- MOMENTS  -->
<!-- ======================================================================= -->
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript">

$('#hora').datetimepicker({
  format: 'LT'
});

</script>
