<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",6) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  3px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



    <!-- ======================================================================= -->
    <!--  titulo geral -->
    <!-- ======================================================================= -->
    <div class="container">
      <div class="row top30 titulo_emp">
        <div class="col-xs-12 text-center">
          <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_personalizada.png" alt="">
          <div class="top25">
            <h2>ANUNCIE EM NOSSA</h2>
            <h3 class="text-capitalize">Tv Corporativa</h3>
          </div>
          <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_personalizada.png" alt="">
        </div>

      </div>
    </div>
  </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->






  <div class="container top60">
    <div class="row empresa top120">

      <!-- ======================================================================= -->
      <!-- conheca nossa empresa  -->
      <!-- ======================================================================= -->
      <div class="col-xs-6">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5);?>

        <div class="desc_empresa">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>

      </div>
      <div class="col-xs-6">
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/bg_tv01.jpg" alt="">
      </div>
      <!-- ======================================================================= -->
      <!-- conheca nossa empresa  -->
      <!-- ======================================================================= -->


      <!-- ======================================================================= -->
      <!-- COMO ANUNCIAR  -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 ">
        <div class="text-center">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>
          <div class="top90">
            <h1><?php Util::imprime($row[titulo]); ?></h1>
            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_empresa_home.jpg" alt="">
          </div>

          <div class="top25">
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>

        </div>
      </div>
      <!-- ======================================================================= -->
      <!-- COMO ANUNCIAR  -->
      <!-- ======================================================================= -->

      <!-- ======================================================================= -->
      <!-- porque ANUNCIAR  -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 ">
        <div class="bg_amarelo pg10 text-center">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4);?>
          <div class="top90">
            <h1><?php Util::imprime($row[titulo]); ?></h1>
            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_empresa_home.jpg" alt="">
          </div>

          <div class="top25 pb40">
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>

        </div>
      </div>
      <!-- ======================================================================= -->
      <!-- porque ANUNCIAR  -->
      <!-- ======================================================================= -->




    </div>
  </div>




  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
