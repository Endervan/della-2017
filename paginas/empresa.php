<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  3px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top30 titulo_emp">
      <div class="col-xs-12 text-center">
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_personalizada.png" alt="">
        <div class="">
          <h2>SAIBA MAIS SOBRE A </h2>
          <h1 class="text-capitalize">della Empório</h1>
        </div>
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_personalizada.png" alt="">
      </div>
      <div class="col-xs-12 top115">
        <h5>Nossa História</h5>
      </div>
    </div>
  </div>
</div>
</div>
<!-- ======================================================================= -->
<!--  titulo -->
<!-- ======================================================================= -->




<div class="container">
  <div class="row empresa ">

    <!-- ======================================================================= -->
    <!-- conheca nossa empresa  -->
    <!-- ======================================================================= -->
    <div class="col-xs-6 top20">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>

      <div class="desc_empresa">
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>

    </div>

    <div class="col-xs-6">
      <!-- ======================================================================= -->
      <!-- slider empresa  -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/slider_empresa.php') ?>

      <!-- ======================================================================= -->
      <!-- slider empresa  -->
      <!-- ======================================================================= -->




    </div>
    <!-- ======================================================================= -->
    <!-- conheca nossa empresa  -->
    <!-- ======================================================================= -->





  </div>
</div>

<div class="top50">  </div>

<!-- ======================================================================= -->
<!-- onde estamos    -->
<!-- ======================================================================= -->
<?php require_once('./includes/onde.php') ?>
<!-- ======================================================================= -->
<!-- onde estamos    -->
<!-- ======================================================================= -->





<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
