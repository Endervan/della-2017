<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_lojas", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",9) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  3px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top50">
      <div class="col-xs-12 titulo_emp text-center">
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_personalizada.png" alt="">
        <div class="top25">
          <h2>CONHEÇA A </h2>
          <h1 class="text-capitalize" ><?php Util::imprime($dados_dentro[titulo]); ?></h1>
        </div>
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_personalizada.png" alt="">
      </div>
    </div>
  </div>
</div>
</div>
<!-- ======================================================================= -->
<!--  titulo -->
<!-- ======================================================================= -->





<div class="container">
  <div class="row">

    <!-- ======================================================================= -->
    <!-- contatos lojas -->
    <!-- ======================================================================= -->


    <div class="col-xs-6 loja top120">


      <h1>
        <span>
          <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_telefone_loja.png" alt="">
          <?php Util::imprime($dados_dentro[ddd1]); ?> <?php Util::imprime($dados_dentro[telefone1]); ?>
          <span class="left15">
            <?php if (!empty($dados_dentro[telefone2])): ?>
              <?php Util::imprime($dados_dentro[ddd2]); ?> <?php Util::imprime($dados_dentro[telefone2]); ?>
            <?php endif; ?>
          </span>

        </span>
      </h1>

      <!-- ======================================================================= -->
      <!-- Horario -->
      <!-- ======================================================================= -->
      <?php $lojas = $obj_site->verifica_loja_aberta($dados_dentro[idloja]);; ?>
      <div class="media horario top20">
        <div class="media-left media-middle">
          <img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_agenda.png" alt="">
        </div>
        <div class="media-body">
          <h3 class="media-heading">Horário de Funcionamento
          <span>: <br> <?php Util::imprime($lojas[horario_abertura], 5); ?> às <?php Util::imprime($lojas[horario_fechamento], 5); ?></span>
          </h3>
        </div>
      </div>
      <!-- ======================================================================= -->
      <!-- horario  -->
      <!-- ======================================================================= -->

      <!-- ======================================================================= -->
      <!-- horario  -->
      <!-- ======================================================================= -->
      <div class="media horario top20">
        <div class="media-left media-middle">
          <img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_localizacao01.png" alt="">
        </div>
        <div class="media-body">
          <h3 class="media-heading">
            <span><?php Util::imprime($dados_dentro[endereco]); ?>
            </span>
          </h3>
        </div>
      </div>
      <!-- ======================================================================= -->
      <!-- horario  -->
      <!-- ======================================================================= -->

      <div class="clearfix">  </div>
      <div class="top20">  </div>
      <a class="btn_fale_loja " href="<?php echo Util::caminho_projeto(); ?>/fale-conosco">
        <img  src="<?php echo Util::caminho_projeto(); ?>/imgs/tire_duvidas.png" alt="">
      </a>


    </div>



    <div class="col-xs-6 top80">

      <!-- ======================================================================= -->
      <!-- SLIDER CATEGORIA -->
      <!-- ======================================================================= -->
      <div class="slider_prod_geral top20">

        <!-- Place somewhere in the <body> of your page -->
        <div id="slider" class="flexslider">
          <ul class="slides slider-prod">
            <?php
            $result = $obj_site->select("tb_galerias_lojas", "AND id_loja = '$dados_dentro[0]' ");
            if(mysql_num_rows($result) > 0)
            {
              while($row = mysql_fetch_array($result)){
                ?>
                <li class="zoom">
                  <a href="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $row[imagem]; ?>" class="group4">
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" class="input100" />
                  </a>
                </li>
                <?php
              }
            }
            ?>

            <!-- items mirrored twice, total of 12 -->
          </ul>
          <span>clique na imagem para Ampliar</span>
        </div>


      </div>
      <!-- ======================================================================= -->
      <!-- SLIDER CATEGORIA -->
      <!-- ======================================================================= -->

    </div>
    <!-- ======================================================================= -->
    <!-- contatos lojas -->
    <!-- ======================================================================= -->


    <!-- ======================================================================= -->
    <!-- mapa   -->
    <!-- ======================================================================= -->
    <div class="col-xs-12 top50">
      <div class="mapa">
        <div class="text-center">
          <h1>COMO CHEGAR</h1>
          <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_empresa_home.jpg" alt="" >
        </div>
        <iframe src="<?php Util::imprime($dados_dentro[src_place]); ?>" width="100%" height="396" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- mapa   -->
    <!-- ======================================================================= -->

  </div>
</div>







<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<script>
$(window).load(function() {
  // The slider being synced must be initialized first

  $('#slider').flexslider({
    animation: "slide",
    controlNav: true,
    animationLoop: false,
    slideshow: true,
    sync: "#carousel"
  });
});


</script>
