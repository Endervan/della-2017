<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 11);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",6) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  3px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top30 titulo_emp">
      <div class="col-xs-12 text-center">
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_personalizada.png" alt="">
        <div class="top25">
          <h2>CONHEÇA NOSSOS</h2>
          <h3 class="text-capitalize">Serviços</h3>
        </div>
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_personalizada.png" alt="">
      </div>

    </div>
  </div>
</div>
</div>
<!-- ======================================================================= -->
<!--  titulo -->
<!-- ======================================================================= -->





<!-- ======================================================================= -->
<!-- CARDAPIO    -->
<!-- ======================================================================= -->
<div class="container top150">
  <div class="row cardapio_home">

    <ul >
      <?php
      $i=0;
      $result = $obj_site->select("tb_servicos");
      if (mysql_num_rows($result) > 0) {

        while ($row = mysql_fetch_array($result)) {
          ?>
          <li class="col-xs-3 top40 padding0">
            <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 300, 400, array("class"=>"input100 img", "alt"=>"$row[titulo]")) ?>
            </amp-img>
          </a>
        </li>
        <?php
      }

    }
    ?>
  </ul>
</div>
</div>
<!-- ======================================================================= -->
<!-- CARDAPIO    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
