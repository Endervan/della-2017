<div class="padding0">
	<div class="dropdown">
		<a class="btn btn_topo_lojas" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<img  src="<?php echo Util::caminho_projeto(); ?>/imgs/btn_lojas01.png" alt="" />
		</a>
		<div class="dropdown-menu topo_meu_loja pb40 pull-right" aria-labelledby="dropdownMenu1">


			<!-- ======================================================================= -->
			<!-- nossas lojas  -->
			<!-- ======================================================================= -->
			<?php
			$result = $obj_site->select("tb_lojas");
			if(mysql_num_rows($result) > 0){
				while($row = mysql_fetch_array($result)){
					$i=0;

					//  verifica se a loja está aberta
					$lojas = $obj_site->verifica_loja_aberta($row[idloja]);

					?>

					<div class="col-xs-3 top145">
						<div class="fundo_lojas">


							<div class="text-center pt15">
								<span class="pull-left <?php echo $lojas[loja_aberta]; ?>" data-toggle="tooltip" data-placement="bottom" title="<?php Util::imprime($lojas[loja_title]); ?>"></span>
								<h2 class="text-uppercase"><?php Util::imprime($row[titulo]); ?></h2>
							</div>

							<div class="clearfix"></div>


							<!-- ======================================================================= -->
							<!-- telefones  -->
							<!-- ======================================================================= -->
							<div class="media top35">
								<div class="media-left media-middle">
									<img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_telefone_topo.png" alt="">
								</div>
								<div class="media-body">
									<h3 class="media-heading">
										<?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?>
									</h3>
								</div>
							</div>

							<?php if (!empty($row[telefone2])): ?>
								<div class="media">
									<div class="media-left media-middle">
										<i class="fa fa-whatsapp"></i>
									</div>
									<div class="media-body">
										<h3 class="media-heading">
											<?php Util::imprime($row[ddd2]); ?> <?php Util::imprime($row[telefone2]); ?>
										</h3>
									</div>
								</div>
							<?php endif; ?>
							<!-- ======================================================================= -->
							<!-- telefones  -->
							<!-- ======================================================================= -->

							<!-- ======================================================================= -->
							<!-- endereco  -->
							<!-- ======================================================================= -->
							<div class="media top20">
								<div class="media-left media-middle">
									<img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_map_topo.png" alt="">
								</div>
								<div class="media-body">
									<h3 class="media-heading">
										<span><?php Util::imprime($row[endereco]); ?>
										</span>
									</h3>
								</div>
							</div>
							<!-- ======================================================================= -->
							<!-- endereco  -->
							<!-- ======================================================================= -->

							<!-- ======================================================================= -->
							<!-- entrega  -->
							<!-- ======================================================================= -->
							<div class="media horario top20">
								<div class="media-left media-middle">
									<img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_agenda_topo.png" alt="">
								</div>
								<div class="media-body">
									<h3 class="media-heading topo-h3-lojas">Horário de Funcionamento
										<span>: <br> <?php Util::imprime($lojas[horario_abertura], 5); ?> às <?php Util::imprime($lojas[horario_fechamento], 5); ?></span>
									</h3>
								</div>
							</div>
							<!-- ======================================================================= -->
							<!-- entrega  -->
							<!-- ======================================================================= -->

							<div class="padding0">
								<a href="<?php Util::imprime($row[link_maps]); ?>" target="_blank" class="btn btn_localizacao input100">ABRIR NO MAPS</a>
							</div>
							<div class="padding0 top5">
								<a class="btn btn_localizacao input100" href="<?php echo Util::caminho_projeto() ?>/loja/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
									SAIBA MAIS
								</a>
							</div>


						</div>
					</div>





					<?php
					if ($i==3) {
						echo "<div class='clearfix'>  </div>";
					}else {
						$i++;
					}
				}
			}
			?>

			<!-- ======================================================================= -->
			<!-- nossas lojas  -->
			<!-- ======================================================================= -->



		</div>
	</div>

	<!-- ======================================================================= -->
	<!-- nossas lojas  -->
	<!-- ======================================================================= -->
</div>
