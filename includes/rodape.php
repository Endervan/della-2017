<div class="container">
	<div class="row">
		<div class="col-xs-12 top40 text-center">
			<img src="<?php echo Util::caminho_projeto(); ?>/imgs/bg_empresa_home.jpg" alt="" >

		</div>
	</div>
</div>
<div class="container-fluid rodape">
	<div class="row">

		<a href="#" class="scrollup">scrollup</a>


		<div class="container top20 bottom20">
			<div class="row">



				<div class="col-xs-5">

					<!-- ======================================================================= -->
					<!-- LOGO    -->
					<!-- ======================================================================= -->
					<div class="col-xs-6 padding0 top75">
						<a href="<?php echo Util::caminho_projeto() ?>/"  title="início">
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_rodape.png" alt="início" />
						</a>
					</div>
					<!-- ======================================================================= -->
					<!-- LOGO    -->
					<!-- ======================================================================= -->

					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->
					<div class="col-xs-6 top50">
						<div class="">
							<ul class="menu_rodape">
								<li class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>">
									<a  href="<?php echo Util::caminho_projeto() ?>/">HOME</a>
								</li>
								<li class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>">
									<a  href="<?php echo Util::caminho_projeto() ?>/empresa">A DELLA</a>
								</li>


								<li class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>">
									<a  href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a>
								</li>
								<?php /*

								<li  class="<?php if(Url::getURL( 0 ) == "anucies" or Url::getURL( 0 ) == "anucie" ){ echo "active"; } ?>">
									<a href="<?php echo Util::caminho_projeto() ?>/anuncie">ANUNCIE</a>
								</li>
								*/ ?>

								<li  class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico" ){ echo "active"; } ?>">
									<a href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS</a>
								</li>

								<li  class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>">
									<a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a>
								</li>

								<li  class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>">
									<a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO</a>
								</li>


							</ul>

						</div>
					</div>
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->

					<div class="col-xs-12 padding0 top25">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/forma_pagamentos.png" alt="">
					</div>


				</div>

				<div class="col-xs-6 padding0">

					<!-- ======================================================================= -->
					<!-- nossas lojas  -->
					<!-- ======================================================================= -->
					<?php
					$result = $obj_site->select("tb_lojas");
					if(mysql_num_rows($result) > 0){
						while($row = mysql_fetch_array($result)){
							$i=0;
							?>

							<div class="col-xs-6 unidades_rodape top10">
								<div class="col-xs-12 pb40">




									<div class="">
										<h2><?php Util::imprime($row[titulo]); ?></h2>
									</div>


									<!-- ======================================================================= -->
									<!-- telefones  -->
									<!-- ======================================================================= -->
									<div class="media top15">
										<div class="media-left media-middle">
											<img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_telefone_rodape.png" alt="">
										</div>
										<div class="media-body">
											<h3 class="media-heading">
												<?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?>

												<?php if (!empty($row[telefone2])): ?>
													<i class="fa fa-whatsapp left5" aria-hidden="true"></i><?php Util::imprime($row[ddd2]); ?> <?php Util::imprime($row[telefone2]); ?>
												<?php endif; ?>

											</h3>
										</div>
									</div>
									<!-- ======================================================================= -->
									<!-- telefones  -->
									<!-- ======================================================================= -->

									<!-- ======================================================================= -->
									<!-- endereco  -->
									<!-- ======================================================================= -->
									<div class="media top10">
										<div class="media-left media-middle">
											<img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_localizacao.png" alt="">
										</div>
										<div class="media-body">
											<h3 class="media-heading">
												<span><?php Util::imprime($row[endereco]); ?>
												</span>
											</h3>
										</div>
									</div>
									<!-- ======================================================================= -->
									<!-- endereco  -->
									<!-- ======================================================================= -->


								</div>
							</div>

							<?php
							if ($i==3) {
								echo "<div class='clearfix'>  </div>";
							}else {
								$i++;
							}
						}
					}
					?>

				</div>

				<!-- ======================================================================= -->
				<!-- redes sociais    -->
				<!-- ======================================================================= -->
				<div class="col-xs-1 redes text-right top30">
					<?php if ($config[google_plus] != "") { ?>
						<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
							<i class="fa fa-google-plus-official fa-2x right15"></i>
						</a>
					<?php } ?>

					<?php if ($config[facebook] != "") { ?>
						<a href="<?php Util::imprime($config[facebook]); ?>" title="facebook" target="_blank" >
							<i class="fa fa-facebook-official fa-2x top20 right15"></i>
						</a>
					<?php } ?>

					<?php if ($config[instagram] != "") { ?>
						<a href="<?php Util::imprime($config[instagram]); ?>" title="instagram" target="_blank" >
							<i class="fa fa-instagram fa-2x top20 right15"></i>
						</a>
					<?php } ?>

					<a href="http://www.homewebbrasil.com.br" target="_blank">
						<img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
					</a>

				</div>
				<!-- ======================================================================= -->
				<!-- redes sociais    -->
				<!-- ======================================================================= -->




			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row rodape-preto lato">
		<div class="col-xs-12 text-center top10 bottom10">
			<h5>© Copyright DELLA EMPÓRIO</h5>
		</div>
	</div>
</div>
