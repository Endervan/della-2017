

<div class="container-fluid">
  <div class="row topo top15">

    <div class="container">
      <div class="row">

        <div class="col-xs-2 logo">
          <a href="<?php echo Util::caminho_projeto() ?>/" title="início">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="início" class="">
          </a>
        </div>


        <!--  ==============================================================  -->
        <!-- MENU-->
        <!--  ==============================================================  -->
        <div class="col-xs-6 top20 menu">
          <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
          <div id="navbar">
            <ul class="nav navbar-nav navbar-right">
              <li >
                <a class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
                  href="<?php echo Util::caminho_projeto() ?>/">HOME
                </a>
              </li>

              <li >
                <a class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
                  href="<?php echo Util::caminho_projeto() ?>/empresa">A DELLA
                </a>
              </li>


              <li>
                <a class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto" or Url::getURL( 0 ) == "orcamento" ){ echo "active"; } ?>"
                  href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS
                </a>
              </li>

              <li>
                <a class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>"
                  href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS
                </a>
              </li>

              <?php /* ?>
              <li >
                <a class="<?php if(Url::getURL( 0 ) == "anuncie" ){ echo "active"; } ?>"
                  href="<?php echo Util::caminho_projeto() ?>/anuncie">ANUNCIE
                </a>
              </li>
              <?php */ ?>



              <li >
                <a class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>"
                  href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO
                </a>
              </li>

              <!-- <li >
                <a class="<?php //if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>"
                  href="<?php //echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO
                </a>
              </li> -->


            </ul>

          </div><!--/.nav-collapse -->

        </div>
        <!--  ==============================================================  -->
        <!-- MENU-->
        <!--  ==============================================================  -->

        <div class="col-xs-4 padding0 top15">
        <div class="col-xs-12 text-right padding0">
          <!-- ======================================================================= -->
          <!-- MENU  lojas -->
          <!-- ======================================================================= -->
          <?php require_once('./includes/topo_lojas.php') ?>
          <!-- ======================================================================= -->
          <!-- MENU   lojas -->
          <!-- ======================================================================= -->

        </div>



          <!--  ==============================================================  -->
          <!--CARRINHO-->
          <!--  ==============================================================  -->
          <?php /*
          <div class="col-xs-6 dropdown">
            <a class="btn padding0" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <img src="<?php echo Util::caminho_projeto(); ?>/imgs/btn_ocamento.png" alt="" />
            </a>


            <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dropdownMenu1">

              <!--  ==============================================================  -->
              <!--sessao adicionar produtos-->
              <!--  ==============================================================  -->

              <?php
              foreach ($_SESSION[pedido] as $key => $produto):
                $row = $obj_site->select_unico("tb_produtos", "idproduto", $key);
                ?>
                  <div class="lista-itens-carrinho">
                    <div class="col-xs-2">
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                    </div>
                    <div class="col-xs-8">
                      <h1><?php Util::imprime($row[titulo]) ?></h1>
                    </div>
                    <div class="col-xs-1">
                      <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-remove"></i> </a>
                    </div>
                    <div class="col-xs-12"><div class="borda_carrinho top5">  </div> </div>
                  </div>
                <?php endforeach; ?>


              <!--  ==============================================================  -->
              <!--sessao adicionar servicos-->
              <!--  ==============================================================  -->
              <?php /*
              if(count($_SESSION[solicitacoes_servicos]) > 0)
              {
                for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                {
                  $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                  ?>
                  <div class="lista-itens-carrinho">
                    <div class="col-xs-2">
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                    </div>
                    <div class="col-xs-8">
                      <h1><?php Util::imprime($row[titulo]) ?></h1>
                    </div>
                    <div class="col-xs-1">
                      <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                    </div>
                    <div class="col-xs-12"><div class="borda_carrinho top5">  </div> </div>

                  </div>
                  <?php
                }
              }
              




              <div class="col-xs-12 text-right top10 bottom20">
              
                <?php if(count($_SESSION[pedido]) > 0){ ?>
                  <a href="<?php echo Util::caminho_projeto() ?>/pedido" title="ENVIAR ORÇAMENTO" class="btn btn_formulario" >
                    ENVIAR PEDIDO
                  </a>
                <?php }else{ ?>
                    <h1>NENHUM PEDIDO ADICIONADO AO CARRINHO</h1> 
                <?php } ?>

              </div>
            </div>
          </div>
          */?>
          <!--  ==============================================================  -->
          <!--CARRINHO-->
          <!--  ==============================================================  -->



        </div>








      </div>
    </div>
  </div>
</div>
