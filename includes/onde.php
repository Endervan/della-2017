<!-- ======================================================================= -->
<!-- ONDEE ESTAMOS    -->
<!-- ======================================================================= -->
<div class="container-fluid bg_onde">
  <div class="row">
    <div class="container">
      <div class="row">

        <!-- ======================================================================= -->
        <!-- nossas lojas  -->
        <!-- ======================================================================= -->
        <?php
        $result = $obj_site->select("tb_lojas");
        if(mysql_num_rows($result) > 0){
          while($row = mysql_fetch_array($result)){
            $i=0;

            //  verifica se a loja está aberta
            $lojas = $obj_site->verifica_loja_aberta($row[idloja]);
            ?>

            <div class="col-xs-3 top145">
              <div class="fundo_lojas_onde">



                <div class="text-center pt15">
                  <h2><?php Util::imprime($row[titulo]); ?></h2>
                </div>

                <div class="clearfix"></div>


                <!-- ======================================================================= -->
                <!-- telefones  -->
                <!-- ======================================================================= -->
                <div class="media top35">
                  <div class="media-left media-middle">
                    <img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_telefone_topo.png" alt="">
                  </div>
                  <div class="media-body">
                    <h3 class="media-heading">
                      <?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?>
                    </h3>
                  </div>
                </div>

                <?php if (!empty($row[telefone2])): ?>
  								<div class="media">
  									<div class="media-left media-middle">
  										<i class="fa fa-whatsapp"></i>
  									</div>
  									<div class="media-body">
  										<h3 class="media-heading">
  											<?php Util::imprime($row[ddd2]); ?> <?php Util::imprime($row[telefone2]); ?>
  										</h3>
  									</div>
  								</div>
  							<?php endif; ?>
                <!-- ======================================================================= -->
                <!-- telefones  -->
                <!-- ======================================================================= -->

                <!-- ======================================================================= -->
                <!-- endereco  -->
                <!-- ======================================================================= -->
                <div class="media top20">
                  <div class="media-left media-middle">
                    <img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_map_topo.png" alt="">
                  </div>
                  <div class="media-body">
                    <h3 class="media-heading">
                      <span><?php Util::imprime($row[endereco]); ?>
                      </span>
                    </h3>
                  </div>
                </div>

                <!-- ======================================================================= -->
                <!-- endereco  -->
                <!-- ======================================================================= -->

                <!-- ======================================================================= -->
                <!-- entrega  -->
                <!-- ======================================================================= -->
                <div class="media horario top20">
                  <div class="media-left media-middle">
                    <img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_agenda_topo.png" alt="">
                  </div>
                  <div class="media-body">
                    <h3 class="media-heading">Horário de entrega
                      <span>: <br> <?php Util::imprime($lojas[horario_abertura], 5); ?> às <?php Util::imprime($lojas[horario_fechamento], 5); ?>
                      </span>
                    </h3>
                  </div>
                </div>
                <!-- ======================================================================= -->
                <!-- entrega  -->
                <!-- ======================================================================= -->

                <div class="padding0">
                  <a href="<?php Util::imprime($row[link_maps]); ?>" target="_blank" class="btn btn_localizacao input100">ABRIR NO MAPS</a>
                </div>


              </div>
            </div>





            <?php
            if ($i==3) {
              echo "<div class='clearfix'>  </div>";
            }else {
              $i++;
            }
          }
        }
        ?>

        <!-- ======================================================================= -->
        <!-- nossas lojas  -->
        <!-- ======================================================================= -->



      </div>
    </div>
  </div>
</div>

<!-- ======================================================================= -->
<!-- ONDEE ESTAMOS    -->
<!-- ======================================================================= -->
