<?php
ob_start();
session_start();

class Produto_Model extends Dao
{

	private $nome_tabela = "tb_produtos";
	private $chave_tabela = "idproduto";
	public $obj_imagem;


	/*	==================================================================================================================	*/
	#	CONSTRUTOR DA CLASSE
	/*	==================================================================================================================	*/
	public function __construct()
	{
		$this->obj_imagem = new Imagem();
		parent::__construct();

	}


	/*	==================================================================================================================	*/
	/*	ORDENA	*/
	/*	==================================================================================================================	*/
	public function atualizar_ordem($ordem){
		if(count($ordem) > 0):
			foreach($ordem as $key=>$orde):
				$sql = "UPDATE ". $this->nome_tabela ." SET ordem = '$orde' WHERE ". $this->chave_tabela ." = $key ";
				parent::executaSQL($sql);
			endforeach;
		endif;
	}


	


	/*  ==================================================================================================================  */
	/*  VERIFICA SE A CATEGORIA ESTÁ SELECIONADA  */
	/*  ==================================================================================================================  */
	public function verifica_categoria_produto($id_categoriaproduto, $idproduto){
		$sql = "SELECT * FROM tb_produtos_categorias WHERE id_categoriaproduto = '$id_categoriaproduto' and id_produto = '$idproduto'  ";
		$result = parent::executaSQL($sql);

		if (mysql_num_rows($result) > 0) {
			return 'selected';
		}else{
			return '';
		}
	}







	/*	==================================================================================================================	*/
	/*	FORMULARIO	*/
	/*	==================================================================================================================	*/
	public function formulario($dados)
	{
		$obj_jquery = new Biblioteca_Jquery();
		$obj_site = new Site();
		?>

		<script type="text/javascript" language="javascript">
		jQuery(function($){
			$(".moeda").maskMoney({showSymbol:false, symbol:"R$", decimal:",", thousands:"."});
		});
		</script>


		<script>
			$(document).ready(function() {
				var max_fields      = 10; //maximum input boxes allowed
				var wrapper         = $(".input_fields_wrap"); //Fields wrapper
				var add_button      = $(".add_field_button"); //Add button ID

				var x = 1; //initlal text box count
				$(add_button).click(function(e){ //on add input button click
					e.preventDefault();
					if(x < max_fields){ //max input box allowed
						x++; //text box increment
						$(wrapper).append('<div class="clearfix"><div class="col-xs-3 pull-left top20">Título:<input class="form-control fundo-form1 input100 mascara-valor" type="text" name="titulo_itens[]"></div><div class="col-xs-3 pull-left top20">Valor:<input class="form-control fundo-form1 input100 mascara-valor" pattern="([0-9]{1,3}\.)?[0-9]{1,3},[0-9]{2}$" type="text" pattern="([0-9]{1,3}\.)?[0-9]{1,3},[0-9]{2}$" name="valor_itens[]"></div><div class="col-xs-3 top20 pull-left">Valor embalagem:<input class="form-control fundo-form1 input100 mascara-valor" type="text" name="valor_embalagem[]" ></div><a href="#" class="remove_field btn-excluir-campo btn btn-danger top40">Excluir</a></div>'); //add input box
					}
				});

				$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
					e.preventDefault(); $(this).parent('div').remove(); x--;
				})
			});
		</script>



		<script type="text/javascript">
		$(document).ready(function(){
			$('#id_tipoveiculo').change(function(){
				$('#modelos_aceitos').load('carrega_modelos.php?id='+$('#id_tipoveiculo').val()+'&id_produto=<?php echo $_GET[id] ?>');
			});

			$('#modelos_aceitos').load('carrega_modelos.php?id='+$('#id_tipoveiculo').val()+'&id_produto=<?php echo $_GET[id] ?>');
		});
		</script>



		<div class="col-xs-5 form-group pt0">
			<label>Categoria</label>
			<?php Util::cria_select_bd("tb_categorias_produtos", "idcategoriaproduto", "titulo", "id_categoriaproduto", $dados[id_categoriaproduto], "form-control fundo-form1 input100") ?>
		</div>

		<div class="col-xs-2 form-group ">
			<label>Exibir na home</label>
			<?php Util::get_sim_nao("exibir_home", $dados[exibir_home], 'form-control fundo-form1 input100') ?>
		</div>

		<div class="col-xs-2 form-group ">
			<label>Obrigatório Quantidade</label>
			<?php Util::get_sim_nao("obrigatorio_quantidade", $dados[obrigatorio_quantidade], 'form-control fundo-form1 input100') ?>
		</div>

		<div class="clearfix">	</div>



		<div class="col-xs-12 form-group ">
			<label>Título</label>
			<input type="text" name="titulo" value="<?php Util::imprime($dados[titulo]) ?>" class="form-control fundo-form1 input100" >
		</div>



		<div class="col-xs-12 form-group ">
			<label>Imagem da capa  - <span>	Recomedado ( 380x270px )</span> </label>
			<?php
			if(!empty($dados[imagem])){
				?>
				<input type="file" name="imagem" id="imagem" value="<?php Util::imprime($dados[imagem]) ?>" class="form-control fundo-form1 input100" />
				<label>Atual:</label>
				<div class="clearfix"></div>
				<img class="col-xs-1" src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($dados[imagem]) ?>" />
			<?php } else { ?>
				<input type="file" name="imagem" id="imagem" value="<?php Util::imprime($dados[imagem]) ?>" class="form-control fundo-form1 input100" />
			<?php } ?>
		</div>



		<div class="col-xs-12 form-group ">
			<label>Valores<span>Informa os valores dos produtos, exemplo: Grande, Pequena</label>

			<?php

			//  verifico se exixtem itens cadastrados
			 $sql = "select * from tb_produtos_precos where id_produto = '$_GET[id]'";
			 $result = parent::executaSQL($sql);
			 if (mysql_num_rows($result) == 0) {
			 ?>
				<div class="input50 top15 bottom15">

					<div class="col-xs-3">Título:<input class="form-control fundo-form1" type="text" name="titulo_itens[]"></div>
					<div class="col-xs-3">Valor:<input class="form-control fundo-form1 mascara-valor" type="text" name="valor_itens[]"></div>
					<div class="col-xs-3">Valor embalagem:<input class="form-control fundo-form1 mascara-valor" type="text" name="valor_embalagem[]" ></div>
				</div>
				<div class="clearfix"></div>
				<div class="input_fields_wrap"></div>
			 <?php
			 }else{
				while ($row = mysql_fetch_array($result)) {
				?>
					<div class="input50 top15 bottom15">

						<div class="col-xs-3">Título:<input class="form-control fundo-form1" type="text" name="titulo_itens[]" value="<?php Util::imprime($row[titulo]) ?>" ></div>
						<div class="col-xs-3">Valor:<input class="form-control fundo-form1 mascara-valor" type="text" name="valor_itens[]" value="<?php echo Util::formata_moeda($row[valor]) ?>" ></div>
						<div class="col-xs-3">Valor embalagem:<input class="form-control fundo-form1 mascara-valor" type="text" name="valor_embalagem[]" value="<?php echo Util::formata_moeda($row[valor_embalagem]) ?>" ></div>
					</div>
					<div class="clearfix"></div>
				<?php
				}

				echo '<div class="input_fields_wrap"></div>';
			 }
			?>




			<div class="col-xs-12">
					<button class="add_field_button top15 btn btn-primary">Adicionar mais campo</button>
			</div>


		</div>




		<div class="col-xs-12 form-group ">
			<label>Selecione os agrupamentos<span></span></label> <br>

			<?php
			//  verifico se exixtem itens cadastrados
			 $sql = "select * from tb_agrupamentos";
			 $result = parent::executaSQL($sql);
			 if (mysql_num_rows($result) > 0) {
				while ($row = mysql_fetch_array($result)) {
					//  verifico se o agrupamento esta selecionado
					$sql1 = "select * from tb_produtos_agrupamentos where id_produto = '$_GET[id]' and id_agrupamento = '$row[idagrupamento]' ";
					$result1 = parent::executaSQL($sql1);
					if (mysql_num_rows($result1) > 0) {
						$checked = 'checked';
					}else{
						$checked = '';
					}

				?>
				<div class="col-xs-4 pull-left">
					<input style="width: 20px;" <?php echo $checked ?> type="checkbox" name="agrupamentos[]" value="<?php Util::imprime($row[0]); ?>"><?php Util::imprime($row[titulo]); ?>
				</div>
				<?php
				}
			 }
			 ?>
		</div>






		<div class="col-xs-12 form-group ">
			<label>Descrição</label>
			<?php $obj_jquery->ckeditor('descricao', $dados[descricao]); ?>
		</div>


		<!-- ======================================================================= -->
		<!-- GOOGLE SEO    -->
		<!-- ======================================================================= -->
		<?php require_once("../includes/google_seo.php"); ?>
		<!-- ======================================================================= -->
		<!-- GOOGLE SEO    -->
		<!-- ======================================================================= -->





		<!-- ======================================================================= -->
		<!-- VALIDACAO DO FORMULARIO    -->
		<!-- ======================================================================= -->
		<script>
		$(document).ready(function() {
			$('.FormPrincipal').bootstrapValidator({
				message: 'This value is not valid',
				feedbackIcons: {
					valid: 'fa-ok',
					invalid: 'fa-remove',
					validating: 'fa-refresh'
				},
				fields: {


					titulo: {
						validators: {
							notEmpty: {

							}
						}
					},

					preco: {
						validators: {
							notEmpty: {

							}
						}
					},

					id_categoriaproduto: {
						validators: {
							notEmpty: {

							}
						}
					},


					garantia: {
						validators: {
							notEmpty: {

							}
						}
					},


					indice_velocidade: {
						validators: {
							notEmpty: {

							}
						}
					},

					exibir_home: {
						validators: {
							notEmpty: {

							}
						}
					},

					id_tipoveiculo: {
						validators: {
							notEmpty: {

							}
						}
					},


					id_marcaveiculo: {
						validators: {
							notEmpty: {

							}
						}
					},

					id_modeloveiculo: {
						validators: {
							notEmpty: {

							}
						}
					},

					ano_inicial: {
						validators: {
							notEmpty: {

							}
						}
					},

					ano_final: {
						validators: {
							notEmpty: {

							}
						}
					},

					indice_carga: {
						validators: {
							notEmpty: {

							}
						}
					},


					largura: {
						validators: {
							notEmpty: {

							}
						}
					},


					altura: {
						validators: {
							notEmpty: {

							}
						}
					},

					aro: {
						validators: {
							notEmpty: {

							}
						}
					},


					medida: {
						validators: {
							notEmpty: {

							}
						}
					},


					valor: {
						validators: {
							notEmpty: {

							},
							numeric: {
								transformer: function($field, validatorName, validator) {
									var value = $field.val();
									return value.replace(',', '');
								}
							}
						}
					},


					email: {
						validators: {
							notEmpty: {

							},
							emailAddress: {
								message: 'Esse endereço de email não é válido'
							}
						}
					},

					mensagem: {
						validators: {
							notEmpty: {

							}
						}
					}
				}
			});
		});
		</script>




		<?php
	}




	/*	==================================================================================================================	*/
	/*	EFETUA O CADASTRO	*/
	/*	==================================================================================================================	*/
	public function cadastra($dados)
	{
		//	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
		if($_FILES[imagem][name] != "")
		{
			//	EFETUO O UPLOAD DA IMAGEM
			$dados[imagem] = Util::upload_imagem("../../uploads", $_FILES[imagem], "4194304");

			//  CRIO O CROP
			$this->efetua_crop_imagem($dados[imagem], 236, 212);
		}

		//	CADASTRA O USUARIO
		$id = parent::insert($this->nome_tabela, $dados);


		// cadastra os valores
		$this->atualiza_preco($id, $dados);

		// cadastra os agrupamentos
		$this->cadastra_agrupamentos($id, $dados);

		//	ARMAZENA O LOG
		parent::armazena_log("tb_logs_logins", "CADASTRO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);


		Util::script_msg("Cadastro efetuado com sucesso.");
		Util::script_location(dirname($_SERVER['SCRIPT_NAME'])."/cadastra.php");

	}




	/*	==================================================================================================================	*/
	/*	EFETUA A ALTERACAO	*/
	/*	==================================================================================================================	*/
	public function altera($id, $dados)
	{
		//	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
		if($_FILES[imagem][name] != "")
		{
			//	EFETUO O UPLOAD DA IMAGEM
			$dados[imagem] = Util::upload_imagem("../../uploads", $_FILES[imagem], "4194304");


			//  CRIO O CROP
			$this->efetua_crop_imagem($dados[imagem], 236, 212);
		}

		parent::update($this->nome_tabela, $id, $dados);

		// cadastra os valores
		$this->atualiza_preco($id, $dados);

		// cadastra os agrupamentos
		$this->cadastra_agrupamentos($id, $dados);


		//	ARMAZENA O LOG
		parent::armazena_log("tb_logs_logins", "ALTERAÇÃO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);



		Util::script_msg("Alterado com sucesso.");
		Util::script_location(dirname($_SERVER['SCRIPT_NAME']));

	}



	public function atualiza_preco($idproduto, $dados){

        //  apago todos os valores antigos
        $sql = "DELETE FROM tb_produtos_precos WHERE id_produto = '$idproduto' ";
        parent::executaSQL($sql);


        //  atualizo os valores
        //  CADASTO OS ITENS
        if(count($dados[titulo_itens]) > 0){
            foreach ($dados[titulo_itens] as $key => $titulo) {

                if (!empty($titulo)) {
                    $titulo = Util::trata_dados_formulario($titulo);
                    $valor = Util::formata_moeda($dados[valor_itens][$key], 'banco');
                    $valor_embalagem = Util::formata_moeda($dados[valor_embalagem][$key], 'banco');
                    $sql = "insert into tb_produtos_precos (id_produto, titulo, valor, valor_embalagem) values ('$idproduto', '$titulo', '$valor', '$valor_embalagem')";
                    parent::executaSQL($sql);
                }

            }
        }



    }



    public function cadastra_agrupamentos($id, $dados){

        //  apago todos os valores antigos
        $sql = "DELETE FROM tb_produtos_agrupamentos WHERE id_produto = '$id' ";
        parent::executaSQL($sql);


        //  atualizo os valores
        if(count($dados[agrupamentos]) > 0){
            foreach ($dados[agrupamentos] as $key => $agrupamento) {


                if (!empty($agrupamento)) {
                    $sql = "insert into tb_produtos_agrupamentos (id_produto, id_agrupamento) values ('$id', '$agrupamento')";
                    parent::executaSQL($sql);
                }

            }
        }


    }




	/*	==================================================================================================================	*/
	/*	ATIVA OU DESATIVA	*/
	/*	==================================================================================================================	*/
	public function ativar_desativar($id, $ativo)
	{
		if($ativo == "SIM")
		{
			$sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'NAO' WHERE " . $this->chave_tabela. " = '$id'";
			parent::executaSQL($sql);

			//	ARMAZENA O LOG
			parent::armazena_log("tb_logs_logins", "DESATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
		}
		else
		{
			$sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'SIM' WHERE " . $this->chave_tabela. " = '$id'";
			parent::executaSQL($sql);

			//	ARMAZENA O LOG
			parent::armazena_log("tb_logs_logins", "ATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
		}
	}




	/*	==================================================================================================================	*/
	/*	EXCLUI	*/
	/*	==================================================================================================================	*/
	public function excluir($id)
	{
		//	BUSCA OS DADOS
		$row = $this->select($id);

		$sql = "DELETE FROM " . $this->nome_tabela. " WHERE " . $this->chave_tabela. " = '$id'";
		parent::executaSQL($sql);

		//	ARMAZENA O LOG
		parent::armazena_log("tb_logs_logins", "EXCLUSÃO DO LOGIN $id, NOME: $row[nome], Email: $row[email]", $sql, $_SESSION[login][idlogin]);
	}




	/*	==================================================================================================================	*/
	/*	BUSCA OS DADOS	*/
	/*	==================================================================================================================	*/
	public function select($id = "")
	{
		if($id != "")
		{
			$sql = "
			SELECT
			*
			FROM
			" . $this->nome_tabela. "
			WHERE
			" . $this->chave_tabela. " = '$id'
			";
			return mysql_fetch_array(parent::executaSQL($sql));
		}
		else
		{
			$sql = "
			SELECT
			*
			FROM
			" . $this->nome_tabela. "
			ORDER BY
			ordem desc
			";
			return parent::executaSQL($sql);
		}

	}


	/*	==================================================================================================================	*/
	/*	EFETUA CROP DA IMAGEM	*/
	/*	==================================================================================================================	*/
	public function efetua_crop_imagem($nome_arquivo, $largura, $altura, $alias = 'tumb_', $tipo = 'crop')
	{
		$obj_imagem = new m2brimagem("../../uploads/$nome_arquivo");
		$obj_imagem->redimensiona($largura, $altura, $tipo);
		$obj_imagem->grava("../../uploads/".$alias.$nome_arquivo);
	}




}
?>
