<?php
ob_start();
session_start();

class Agrupamento_Model extends Dao
{

	private $nome_tabela = "tb_agrupamentos";
	private $chave_tabela = "idagrupamento";
	public $obj_imagem;


	/*	==================================================================================================================	*/
	#	CONSTRUTOR DA CLASSE
	/*	==================================================================================================================	*/
	public function __construct()
	{
            $this->obj_imagem = new Imagem();
            parent::__construct();
	}


	/*	==================================================================================================================	*/
	/*	ORDENA	*/
	/*	==================================================================================================================	*/
	public function atualizar_ordem($ordem){

            if(count($ordem) > 0):

                foreach($ordem as $key=>$orde):

                    $sql = "UPDATE ". $this->nome_tabela ." SET ordem = '$orde' WHERE ". $this->chave_tabela ." = $key ";
                    parent::executaSQL($sql);

                endforeach;

            endif;

	}


	/*	==================================================================================================================	*/
	/*	FORMULARIO	*/
	/*	==================================================================================================================	*/
	public function formulario($dados)
	{
            $obj_jquery = new Biblioteca_Jquery();
            ?>
            <script language="javascript">
                $(document).ready(function() {
                    jQuery(function($){
                       $("#data_atendimento").mask("99/99/9999");
                    });
                });
            </script>


            <script>
                $(document).ready(function() {
                    var max_fields      = 100000; //maximum input boxes allowed
                    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
                    var add_button      = $(".add_field_button"); //Add button ID

                    var x = 1; //initlal text box count
                    $(add_button).click(function(e){ //on add input button click
                        e.preventDefault();
                        if(x < max_fields){ //max input box allowed
                            x++; //text box increment
                            $(wrapper).append('<div class="clearfix top15"> <div class="col-xs-3 tam3-linhas pull-left">Título:<input class="form-control fundo-form1 input100 mascara-valor" type="text" name="titulo_itens[]"></div>  <div class="col-xs-3 tam50 pull-left">Subtexto:<input class="form-control fundo-form1 input100 mascara-valor" type="text" name="subtexto[]"></div>  <div class="col-xs-3 tam1-linhas pull-left">Valor:<input class="form-control fundo-form1 input100 mascara-valor" pattern="([0-9]{1,3}\.)?[0-9]{1,3},[0-9]{2}$" type="text" pattern="([0-9]{1,3}\.)?[0-9]{1,3},[0-9]{2}$" name="valor_itens[]"></div><a href="#" class="remove_field btn-excluir-campo btn btn-danger top20 left10">Excluir</a></div>'); //add input box
                        }
                    });

                    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                        e.preventDefault(); $(this).parent('div').remove(); x--;
                    })
                });
            </script>


            <script language="javascript">
                $(document).ready(function() {
                    jQuery(function($){
                       //$(".mascara-valor").maskMoney({showSymbol:false, symbol:"R$", decimal:",", thousands:"."});
                    });
                });
            </script>




            <div class="col-xs-12 form-group ">
                <label>Título<span></span></label>
                <input type="text" name="titulo" id="titulo" value="<?php Util::imprime($dados[titulo]) ?>" class="form-control fundo-form1 input100" />
            </div>


			<div class="col-xs-12 form-group ">
                <label>Título para o cliente <span>Título que será exibido para o cliente. Ex.: Opções de recheio(até 2). Opções de mistura. Opções de Cobertura.</span></label>
                <input type="text" name="titulo_cliente" id="titulo_cliente" value="<?php Util::imprime($dados[titulo_cliente]) ?>" class="form-control fundo-form1 input100" />
            </div>

			<div class="col-xs-12 form-group ">
                <label>Título abreviado o cliente <span>Título abreviado do agrupamento. Ex.: Recheio. Mistura. Cobertura.</span></label>
                <input type="text" name="titulo_abreviado" id="titulo_abreviado" value="<?php Util::imprime($dados[titulo_abreviado]) ?>" class="form-control fundo-form1 input100" />
            </div>

			<div class="col-xs-12 form-group ">
                <label>Obrigatório <span>Especifica se o usuário é obrigado a selecionar o grupo ou opcional.</span></label>
				<select name="obrigatorio" id="obrigatorio" class="form-control fundo-form1 input100">
					<option value="">Selecione</option>
					<option value="SIM" <?php if($dados[obrigatorio] == 'SIM'){ echo 'selected'; } ?> >SIM</option>
					<option value="NAO" <?php if($dados[obrigatorio] == 'NAO'){ echo 'selected'; } ?> >NÃO</option>
				</select>
            </div>

			<div class="col-xs-12 form-group ">
                <label>Quantidade selecionáveis <span>Aqui o usuário informa a quantidade de itens que podem ser selecionados</span></label>
                <input type="number" min="1" name="qtd_selecionaveis" id="qtd_selecionaveis" value="<?php Util::imprime($dados[qtd_selecionaveis]) ?>" class="form-control fundo-form1 input100" />
            </div>


			<div class="col-xs-12 form-group ">
				<?php $dias_semana = unserialize($dados[dias_disponiveis]); ?>
				<label>Dias disponíveis <span>Aqui o usuário informa os dias que o agrupamento estará disponível para seleção</span></label>
				<br>

				<div class="right20 pull-left">
					<label for=""><input type="checkbox" name="dias_disponiveis[]" value="0" <?php if (in_array(0, $dias_semana)) { echo "checked"; } ?> style="width: 20px !important;">Domingo</label>
				</div>
				<div class="right20 pull-left">
					<label for=""><input type="checkbox" name="dias_disponiveis[]" value="1" <?php if (in_array(1, $dias_semana)) { echo "checked"; } ?> style="width: 20px !important;">Segunda-Feira</label>
				</div>
				<div class="right20 pull-left">
					<label for=""><input type="checkbox" name="dias_disponiveis[]" value="2" <?php if (in_array(2, $dias_semana)) { echo "checked"; } ?> style="width: 20px !important;">Terça-Feira</label>
				</div>
				<div class="right20 pull-left">
					<label for=""><input type="checkbox" name="dias_disponiveis[]" value="3" <?php if (in_array(3, $dias_semana)) { echo "checked"; } ?> style="width: 20px !important;">Quarta-Feira</label>
				</div>
				<div class="right20 pull-left">
					<label for=""><input type="checkbox" name="dias_disponiveis[]" value="4" <?php if (in_array(4, $dias_semana)) { echo "checked"; } ?> style="width: 20px !important;">Quinta-Feira</label>
				</div>
				<div class="right20 pull-left">
					<label for=""><input type="checkbox" name="dias_disponiveis[]" value="5" <?php if (in_array(5, $dias_semana)) { echo "checked"; } ?> style="width: 20px !important;">Sexta-Feira</label>
				</div>
				<div class="right20 pull-left">
					<label for=""><input type="checkbox" name="dias_disponiveis[]" value="6" <?php if (in_array(0, $dias_semana)) { echo "checked"; } ?> style="width: 20px !important;">Sábado</label>
				</div>

			</div>



			<div class="col-xs-12 form-group ">
                <label>Itens <span></span></label>

				<?php

				//  verifico se exixtem itens cadastrados
				 $sql = "select * from tb_itens_agrupamentos where id_agrupamento = '$_GET[id]'";
				 $result = parent::executaSQL($sql);
				 if (mysql_num_rows($result) == 0) {
				 ?>
					<div class="col-xs-12 input50 top15 bottom15">

						<div class="col-xs-3 tam3-linhas pull-left ">Título:<input class="form-control fundo-form1 input100" type="text" name="titulo_itens[]"></div>
						<div class="col-xs-3 tam50 pull-left ">Subtexto:<input class="form-control fundo-form1 input100" type="text" name="subtexto[]"></div>
						<div class="col-xs-3 tam1-linhas pull-left">Valor:<input class="form-control fundo-form1 input100 mascara-valor" type="text" name="valor_itens[]"></div>
					</div>
					<div class="input_fields_wrap col-xs-12">

					</div>
				 <?php
				 }else{
					while ($row = mysql_fetch_array($result)) {
					?>
						<div class="col-xs-12 input50 top15 bottom15">

							<div class="col-xs-3 tam3-linhas pull-left">Título:<input class="form-control fundo-form1 input100" type="text" name="titulo_itens[]" value="<?php Util::imprime($row[titulo]) ?>" ></div>
							<div class="col-xs-3 tam50 pull-left">Subtexto:<input class="form-control fundo-form1 input100" type="text" name="subtexto[]" value="<?php Util::imprime($row[subtexto]) ?>" ></div>
							<div class="col-xs-3 tam1-linhas pull-left">Valor:<input class="form-control fundo-form1 input100 mascara-valor" type="text" name="valor_itens[]" value="<?php echo Util::formata_moeda($row[valor]) ?>" ></div>
						</div>
					<?php
					}

					echo '<div class="input_fields_wrap"></div>';
				 }
				?>


				<div class="col-xs-12">
					<button class="add_field_button top20 left20 btn btn-primary">Adicionar mais campo</button>
				</div>


            </div>



			<!-- ======================================================================= -->
			<!-- VALIDACAO DO FORMULARIO    -->
			<!-- ======================================================================= -->
			<script>
			$(document).ready(function() {
				$('.FormPrincipal').bootstrapValidator({
					message: 'This value is not valid',
					feedbackIcons: {
						valid: 'fa fa-check',
						invalid: 'fa fa-remove',
						validating: 'fa fa-refresh'
					},
					fields: {


						titulo: {
							validators: {
								notEmpty: {

								}
							}
						},

						titulo_cliente: {
							validators: {
								notEmpty: {

								}
							}
						},

						titulo_abreviado: {
							validators: {
								notEmpty: {

								}
							}
						},

						obrigatorio: {
							validators: {
								notEmpty: {

								}
							}
						},

						qtd_selecionaveis: {
							validators: {
								notEmpty: {

								}
							}
						},





						id_categoriaproduto: {
							validators: {
								notEmpty: {

								}
							}
						},

						email: {
							validators: {
								notEmpty: {

								},
								emailAddress: {
									message: 'Esse endereço de email não é válido'
								}
							}
						},

						mensagem: {
							validators: {
								notEmpty: {

								}
							}
						}
					}
				});
			});
			</script>




            <?php
	}




	/*	==================================================================================================================	*/
	/*	EFETUA O CADASTRO	*/
	/*	==================================================================================================================	*/
	public function cadastra($dados)
	{
            //  SERIALIZA O ARRAY
            $dados[dias_disponiveis] = serialize($dados[dias_disponiveis]);

            //	CADASTRA
            $id = parent::insert($this->nome_tabela, $dados);

            //  CADASTRA OS ITENS
            $this->cadastra_itens($id, $dados);


            //	ARMAZENA O LOG
            parent::armazena_log("tb_logs_logins", "CADASTRO DO AGRUPAMENTOS $dados[nome]", $sql, $_SESSION[login][idlogin]);


            Util::script_msg("Cadastro efetuado com sucesso.");
            Util::script_location(dirname($_SERVER['SCRIPT_NAME'])."/cadastra.php");

	}




	/*	==================================================================================================================	*/
	/*	EFETUA A ALTERACAO	*/
	/*	==================================================================================================================	*/
	public function altera($id, $dados)
	{
            //  SERIALIZA O ARRAY
            $dados[dias_disponiveis] = serialize($dados[dias_disponiveis]);

            parent::update($this->nome_tabela, $id, $dados);

            //  CADASTRA OS ITENS
            $this->cadastra_itens($id, $dados);

            //	ARMAZENA O LOG
            parent::armazena_log("tb_logs_logins", "ALTERAÇÃO DO AGRUPAMENTOS $dados[nome]", $sql, $_SESSION[login][idlogin]);



            Util::script_msg("Alterado com sucesso.");
            Util::script_location(dirname($_SERVER['SCRIPT_NAME']));

	}






	/* ==================================================================================================================  */
    /*  CADASTRA OS INTENS  */
    /*  ==================================================================================================================  */
    public function cadastra_itens($id, $dados)
    {

        // APAGO OS ITENS DO AGRUPAMENTO
        $sql = "delete from tb_itens_agrupamentos where id_agrupamento =  $id";
        parent::executaSQL($sql);

        //  CADASTO OS ITENS
        if(count($dados[titulo_itens]) > 0){
            foreach ($dados[titulo_itens] as $key => $titulo) {

                if (!empty($titulo)) {
                    $titulo = Util::trata_dados_formulario($titulo);
                    $subtexto = Util::trata_dados_formulario($dados[subtexto][$key]);
                    $valor = Util::formata_moeda($dados[valor_itens][$key], 'banco');
                    $sql = "insert into tb_itens_agrupamentos (id_agrupamento, titulo, valor, subtexto) values ('$id', '$titulo', '$valor', '$subtexto')";
                    parent::executaSQL($sql);
                }

            }
        }


    }



	/*	==================================================================================================================	*/
	/*	ATIVA OU DESATIVA	*/
	/*	==================================================================================================================	*/
	public function ativar_desativar($id, $ativo)
	{
            if($ativo == "SIM")
            {
                $sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'NAO' WHERE " . $this->chave_tabela. " = '$id'";
                parent::executaSQL($sql);

                //	ARMAZENA O LOG
                parent::armazena_log("tb_logs_logins", "DESATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
            }
            else
            {
                $sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'SIM' WHERE " . $this->chave_tabela. " = '$id'";
                parent::executaSQL($sql);

                //	ARMAZENA O LOG
                parent::armazena_log("tb_logs_logins", "ATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
            }
	}




	/*	==================================================================================================================	*/
	/*	EXCLUI	*/
	/*	==================================================================================================================	*/
	public function excluir($id)
	{
            //	BUSCA OS DADOS
            $row = $this->select($id);

            $sql = "DELETE FROM " . $this->nome_tabela. " WHERE " . $this->chave_tabela. " = '$id'";
            parent::executaSQL($sql);

            //	ARMAZENA O LOG
            parent::armazena_log("tb_logs_logins", "EXCLUSÃO DO LOGIN $id, NOME: $row[nome], Email: $row[email]", $sql, $_SESSION[login][idlogin]);
	}




	/*	==================================================================================================================	*/
	/*	BUSCA OS DADOS	*/
	/*	==================================================================================================================	*/
	public function select($id = "")
	{
            if($id != "")
            {
                    $sql = "
                            SELECT
                                    *
                            FROM
                                    " . $this->nome_tabela. "
                            WHERE
                                    " . $this->chave_tabela. " = '$id'
                            ";
                    return mysql_fetch_array(parent::executaSQL($sql));
            }
            else
            {
                    $sql = "
                            SELECT
                                    *
                            FROM
                                    " . $this->nome_tabela. "
                            ORDER BY
                                    ordem desc
                            ";
                    return parent::executaSQL($sql);
            }

	}


	/*	==================================================================================================================	*/
	/*	EFETUA CROP DA IMAGEM	*/
	/*	==================================================================================================================	*/
	public function efetua_crop_imagem($nome_arquivo, $largura, $altura, $alias = 'tumb_', $tipo = 'crop')
	{
            $obj_imagem = new m2brimagem("../../uploads/$nome_arquivo");
            $obj_imagem->redimensiona($largura, $altura, $tipo);
            $obj_imagem->grava("../../uploads/".$alias.$nome_arquivo);
	}




}
?>
