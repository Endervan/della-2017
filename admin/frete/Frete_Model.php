<?php
ob_start();
session_start();


class Frete_Model extends Dao
{

	private $nome_tabela = "tb_fretes";
	private $chave_tabela = "idfrete";
	public $obj_imagem;






	/*	==================================================================================================================	*/
	/*	BUSCA OS DADOS	*/
	/*	==================================================================================================================	*/
	public function get_dados($uf, $cidade)
	{
		$sql = "
				SELECT
					*
				FROM
					log_bairro lb, log_localidade ll
				WHERE
					lb.loc_nu_sequencial = ll.loc_nu_sequencial
					AND lb.loc_nu_sequencial = '$cidade'
				ORDER by
					ll.loc_no, lb.ufe_sg, lb.bai_no
				";
		return parent::executaSQL($sql);


	}










	/*	==================================================================================================================	*/
	#	CONSTRUTOR DA CLASSE
	/*	==================================================================================================================	*/
	public function __construct()
	{
		$this->obj_imagem = new Imagem();
		parent::__construct();
	}


	/*	==================================================================================================================	*/
	/*	ORDENA	*/
	/*	==================================================================================================================	*/
	public function atualizar_ordem($valor_frete, $efetua_entrega, $id_loja){


		if(count($valor_frete) > 0):
			foreach($valor_frete as $key=>$valor):
				$valor = Util::formata_moeda($valor, 'banco');
				$sql = "UPDATE log_bairro SET valor_frete = '$valor' WHERE bai_nu_sequencial = $key ";
				parent::executaSQL($sql);
			endforeach;
		endif;



		//	reinicio os locais de entrega
		$sql = "UPDATE log_bairro SET efetua_entrega = ''";
		parent::executaSQL($sql);

		if(count($efetua_entrega) > 0):

			foreach($efetua_entrega as $key=>$valor):
				$sql = "UPDATE log_bairro SET efetua_entrega = '$valor', id_loja = '$id_loja[$key]' WHERE bai_nu_sequencial = $key ";
				parent::executaSQL($sql);
			endforeach;
		endif;

	}


	/*	==================================================================================================================	*/
	/*	FORMULARIO	*/
	/*	==================================================================================================================	*/
	public function formulario($dados)
	{
		$obj_jquery = new Biblioteca_Jquery();
		?>
    	<script language="javascript">
			$(document).ready(function() {
				jQuery(function($){
				   $("#data_atendimento").mask("99/99/9999");
				   $("#valor").maskMoney({showSymbol:false, symbol:"R$", decimal:",", thousands:"."});
				});
			});
		</script>

        <div class="class-form-1">
            <ul>
                <li>
                	<p>Faixa de cep inicial<span>Somente números</span></p>
                    <input type="text" name="faixa_cep_inicio" maxlength="7" id="faixa_cep_inicio" value="<?php Util::imprime($dados[faixa_cep_inicio]) ?>" class="validate[required]" />
                </li>
                <li>
                	<p>Faixa de cep final<span>Somente números</span></p>
                    <input type="text" name="faixa_cep_fim" maxlength="7" id="faixa_cep_fim" value="<?php Util::imprime($dados[faixa_cep_fim]) ?>" class="validate[required]" />
                </li>
                <li>
                	<p>Valor<span></span></p>
                    <input type="text" name="valor" id="valor" value="<?php echo Util::formata_moeda($dados[valor]) ?>" class="validate[required]" />
                </li>
            </ul>
        </div>
    	<?php
	}

	/*	==================================================================================================================	*/
	/*	EFETUA CROP DA IMAGEM	*/
	/*	==================================================================================================================	*/
	public function efetua_crop_imagem($id, $nome_arquivo, $nome_campo, $tamanho_width_tumb, $tamanho_height_tumb, $url_retorno, $msg_sucesso, $tamanho_imagem = 1920)
	{
		//	CRIO O CROP DA IMAGEM
		$nome_tabela = $this->nome_tabela;
		$idtabela = $this->chave_tabela;
		$this->obj_imagem->gera_imagem_crop($id, $nome_arquivo, $nome_tabela, $idtabela, $nome_campo, $tamanho_imagem, $tamanho_width_tumb, $tamanho_height_tumb, $url_retorno, $msg_sucesso);
	}


	/*	==================================================================================================================	*/
	/*	EFETUA O CADASTRO	*/
	/*	==================================================================================================================	*/
	public function cadastra($dados)
	{
		//	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
		if($_FILES[imagem][name] != "")
		{
			//	EFETUO O UPLOAD DA IMAGEM
			$dados[imagem] = Util::upload_imagem("../../uploads", $_FILES[imagem], "4194304",null,null,1920,800);
		}

		//	CADASTRA O USUARIO
		$id = parent::insert($this->nome_tabela, $dados);

		//	ARMAZENA O LOG
		parent::armazena_log("tb_logs_logins", "CADASTRO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);


		//	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
		/*if($_FILES[imagem][name] != "")
		{
			//	EFETUO O CROP DA IMAGEM
			$this->efetua_crop_imagem($id, $dados[imagem], "imagem", 195, 148, $_SERVER['PHP_SELF'], "Cadastro efetuado com sucesso");
			     //efetua_crop_imagem($id, $nome_arquivo, $nome_campo, $tamanho_width_tumb, $tamanho_height_tumb, $url_retorno, $msg_sucesso, $tamanho_imagem = 800)
		}*/

		Util::script_msg("Cadastro efetuado com sucesso.");
		Util::script_location(dirname($_SERVER['SCRIPT_NAME'])."/cadastra.php");

	}




	/*	==================================================================================================================	*/
	/*	EFETUA A ALTERACAO	*/
	/*	==================================================================================================================	*/
	public function altera($id, $dados)
	{
		//	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
		if($_FILES[imagem][name] != "")
		{
			//	EFETUO O UPLOAD DA IMAGEM
			$dados[imagem] = Util::upload_imagem("../../uploads", $_FILES[imagem], "4194304",null,null,1920,800);
		}

		parent::update($this->nome_tabela, $id, $dados);


		//	ARMAZENA O LOG
		parent::armazena_log("tb_logs_logins", "ALTERAÇÃO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);

		//	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
		/*if($_FILES[imagem][name] != "")
		{
			//	EFETUO O CROP DA IMAGEM
			$this->efetua_crop_imagem($id, $dados[imagem], "imagem", 195, 148, $_SERVER['PHP_SELF'], "Cadastro efetuado com sucesso");
		}*/

		Util::script_msg("Alterado com sucesso.");
		Util::script_location(dirname($_SERVER['SCRIPT_NAME'])."/index.php");

	}



	/*	==================================================================================================================	*/
	/*	ATIVA OU DESATIVA	*/
	/*	==================================================================================================================	*/
	public function ativar_desativar($id, $ativo)
	{
		if($ativo == "SIM")
		{
			$sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'NAO' WHERE " . $this->chave_tabela. " = '$id'";
			parent::executaSQL($sql);

			//	ARMAZENA O LOG
			parent::armazena_log("tb_logs_logins", "DESATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
		}
		else
		{
			$sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'SIM' WHERE " . $this->chave_tabela. " = '$id'";
			parent::executaSQL($sql);

			//	ARMAZENA O LOG
			parent::armazena_log("tb_logs_logins", "ATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
		}

	}




	/*	==================================================================================================================	*/
	/*	EXCLUI	*/
	/*	==================================================================================================================	*/
	public function excluir($id)
	{
		//	BUSCA OS DADOS
		$row = $this->select($id);

		$sql = "DELETE FROM " . $this->nome_tabela. " WHERE " . $this->chave_tabela. " = '$id'";
		parent::executaSQL($sql);

		//	ARMAZENA O LOG
		parent::armazena_log("tb_logs_logins", "EXCLUSÃO DO LOGIN $id, NOME: $row[nome], Email: $row[email]", $sql, $_SESSION[login][idlogin]);
	}




	/*	==================================================================================================================	*/
	/*	BUSCA OS DADOS	*/
	/*	==================================================================================================================	*/
	public function select($id = "")
	{
		if($id != "")
		{
			$sql = "
					SELECT
						*
					FROM
						" . $this->nome_tabela. "
					WHERE
						" . $this->chave_tabela. " = '$id'
					";
			return mysql_fetch_array(parent::executaSQL($sql));
		}
		else
		{
			$sql = "
				SELECT
					*
				FROM
					" . $this->nome_tabela. "
				ORDER BY
					ordem desc
				";
			return parent::executaSQL($sql);
		}

	}







}

?>
