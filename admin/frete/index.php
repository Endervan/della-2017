<?php
require_once("../../class/Include.class.php");
require_once("Frete_Model.php");
require_once("../trava.php");

$obj_control = new Frete_Model();
$obj_site = new Site();


//	ATUALIZA A ORDEM
if(isset($_POST['valida_entrega'])):

	$obj_control->atualizar_ordem($_POST['valor_frete'], $_POST['efetua_entrega'], $_POST['id_loja']);

endif;




//  DESATIVA OU ATIVA UM ITEM
if(isset($_GET[action]) and $_GET[action] == "ativar_desativar")
{
  $obj_control->ativar_desativar($_GET[id], $_GET[ativo]);
  //Util::script_msg("Registro alterado com sucesso");
  Util::script_location("index.php");
}


//  EXCLUIR
if(isset($_GET[action]) and $_GET[action] == "excluir")
{
  $obj_control->excluir($_GET[id]);
  //Util::script_msg("Registro excluído com sucesso.");
  Util::script_location("index.php");
}


?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" href="css/style.css" type="text/css" media="all"/>

<?php require_once("../includes/head.php") ?>

<script language="javascript">
	$(document).ready(function() {
		jQuery(function($){
		   $("#data_atendimento").mask("99/99/9999");
		   $(".formato_moeda").maskMoney({showSymbol:false, symbol:"R$", decimal:",", thousands:"."});
		});
	});
</script>

<title>Admin - <?php echo $_SERVER['SERVER_NAME'] ?></title>
</head>

<body>


	<!-- ======================================================================= -->
	<!-- topo	-->
	<!-- ======================================================================= -->
	<?php require_once("../includes/topo.php"); ?>





	<div class="container-fluid">
      <div class="row">


      <!-- ======================================================================= -->
      <!-- LATERAL	-->
      <!-- ======================================================================= -->
      <div class="col-xs-2 sidebar">
      	<?php require_once("../includes/lateral.php"); ?>
      </div>
      <!-- ======================================================================= -->
      <!-- LATERAL	-->
      <!-- ======================================================================= -->





      <!-- ======================================================================= -->
      <!-- CONTEUDO PRINCIPAL	-->
      <!-- ======================================================================= -->
      <div class="col-xs-10 main ">


		  <div class="col-xs-12 bottom20">
			  <form class="" action="" method="get">
				  <div class="pull-left right20 top5">
				  		SELECIONE A UF
				  </div>

				  <div class="pull-left right20">
				  	<?php Util::get_ufs("uf", $_GET[uf], 'form-control fundo-form1', 'onchange="this.form.submit()"') ?>
				  </div>

				  <div class="pull-left">
					  <?php
  					if (!empty($_GET[uf])) {
  						$sql = "SELECT * FROM log_localidade where ufe_sg = '$_GET[uf]'";
  						Util::cria_select_bd_com_sql('cidade', $_GET[cidade], $sql, 'class= "form-control fundo-form1" onchange="this.form.submit()"');
  					}
  					?>
				  </div>



			</form>
		  </div>



		  <?php
		  if (!empty($_GET[uf]) and !empty($_GET[cidade])) {
			  $result = $obj_control->get_dados($_GET[uf], $_GET[cidade]);
		  }

		  //$result = $obj_control->get_dados('DF', 1778);

		  if(mysql_num_rows($result) == 0)
          {
			  if (!empty($_GET[uf]) and !empty($_GET[cidade])) {
		            echo "<h1>Nenhum resultado cadastrado</h1>";
				}
          }
          else
          {
            ?>







            <form action="" method="post" name="form_listagem" id="form_listagem" enctype="multipart/form-data">


				<script type="text/javascript">

				$(document).ready(function(){
					// Forma 1
					var checkTodos = $("#checkTodos");
					checkTodos.click(function () {
					  if ( $(this).is(':checked') ){
					    $('input:checkbox').prop("checked", true);
					  }else{
					    $('input:checkbox').prop("checked", false);
					  }
					});
				});

				</script>

			  <div class="tabela-holder">


                <!-- ======================================================================= -->
                <!-- PESQUISA  -->
                <!-- ======================================================================= -->
				<div class="clearfix top20">
					<?php require_once("../includes/pesquisa_tabelas.php"); ?>
				</div>
                <!-- ======================================================================= -->
                <!-- PESQUISA  -->
                <!-- ======================================================================= -->


				<table class="table table-hover">
                  <thead>
                    <tr>
                      <th>UF</th>
                      <th class="text-center">CIDADE</th>
                      <th class="text-center">BAIRRO</th>
                      <th class="text-center">
						  EFETUA ENTREGA
						  <p><input type="checkbox" id="checkTodos" name="checkTodos"> Selecionar Todos</p>
				  	  </th>
                      <?php /* ?>
					  <th class="text-center">VALOR</th>
                      <th class="text-center">LOJA QUE ATENDE</th>
					  <?php */ ?>
                    </tr>
                  </thead>
                  <tbody class="searchable">
					  <?php
  					while($row = mysql_fetch_array($result))
  					{
  					?>
  					 <tr>
  						 <td class="titulo-componente-tabela">
  							 <?php Util::imprime($row[ufe_sg]); ?>
  						 </td>
  						 <td class="titulo-componente-tabela">
  						   <?php Util::imprime($row[loc_no]); ?>
  						 </td>
  						 <td class="titulo-componente-tabela">
  						   <?php Util::imprime($row[bai_no]); ?>
  						 </td>

  						 <td align="center">
  					 <input type="checkbox" <?php if($row[efetua_entrega] == "on"){ echo 'checked'; } ?> name="efetua_entrega[<?php echo $row[bai_nu_sequencial] ?>]"  />
					 <input type="hidden" name="valida_entrega" value="1">
  				   </td>

				   <?php /* ?>
  				   <td align="center">
  					 <input type="text" class="formato_moeda form-control fundo-form1" name="valor_frete[<?php echo $row[bai_nu_sequencial] ?>]" value="<?php echo Util::formata_moeda($row[valor_frete]) ?>" style="text-align:center; width: 80%;"  />
  				   </td>

  				   <td align="center">

  					 <select name="id_loja[<?php echo $row[bai_nu_sequencial] ?>] "  id="id_loja[]" class="form-control fundo-form1" >
  						 <option value=''>Selecione</option>

  						 <?php
  						 $result1 = $obj_site->select("tb_lojas");
  						 if(mysql_num_rows($result1) > 0){
  							 while ($row1 = mysql_fetch_array($result1)) { ?>
  								 <option <?php if($row[id_loja] == $row1[idloja]){ echo 'selected'; } ?> value="<?php Util::imprime($row1[0]); ?>"><?php Util::imprime($row1[titulo]); ?> </option>}
  								 option
  							 <?php }
  						 }
  						 ?>

  					 </select>


  				   </td>
				   <?php */ ?>


  					 </tr>
  					 <?php
  					}
  					 ?>
				</tbody>
			  </table>

			  <div class="col-xs-12 text-center">
				<input class="btn btn-primary" type="submit" name="btn_enviar" value="ENVIAR" />
			  </div>


			</div>

			<div class="tabela-fundo">
			  &nbsp;
			</div>

		  </form>


               <?php
   			}
   			?>

      </div>
      <!-- ======================================================================= -->
      <!-- CONTEUDO PRINCIPAL	-->
      <!-- ======================================================================= -->







</body>
</html>
