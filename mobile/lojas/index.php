<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 16); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>


</head>





<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <div class="row">
    <!-- ======================================================================= -->
    <!--  titulo geral -->
    <!-- ======================================================================= -->
    <div class="col-12 localizacao-pagina titulo_emp text-center">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_personalizada.png" height="15" width="214" alt=""></amp-img>
      <div class="">
        <h2>CONFIRA</h2>
        <h1 class="text-capitalize">Nossas Lojas</h1>
      </div>
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_personalizada.png" height="15" width="214" alt=""></amp-img>
    </div>
    <!-- ======================================================================= -->
    <!--  titulo -->
    <!-- ======================================================================= -->
  </div>



  <!-- ======================================================================= -->
  <!-- nossas lojas  -->
  <!-- ======================================================================= -->
  <div class="row">
    <?php
    $result = $obj_site->select("tb_lojas");
    if(mysql_num_rows($result) > 0){
      while($row = mysql_fetch_array($result)){
        $i=0;

        //  verifica se a loja está aberta
        $lojas = $obj_site->verifica_loja_aberta($row[idloja]);
        ?>

        <div class="col-12 ">
          <div class="fundo_lojas">

            <div class="text-center pt15">
              <h2 class="text-uppercase"><?php Util::imprime($row[titulo]); ?></h2>
            </div>

            <div class="clearfix"></div>


            <!-- ======================================================================= -->
            <!-- telefones  -->
            <!-- ======================================================================= -->
            <div class="media top35">
              <div class="media-left media-middle">
                <amp-img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_telefone.png" height="24" width="24" alt=""></amp-img>
              </div>
              <div class="media-body">
                <div class="pull-left">
                  <h3 class="media-heading">
                    <?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?>
                  </h3>
                </div>
                <div class="pull-right">
                  <a class="btn btn_telefone" href="tel:+55<?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?>">
                    CHAMAR
                  </a>
                </div>

              </div>
            </div>

            <?php if (!empty($row[telefone2])): ?>
              <div class="media top35">
                <div class="media-left media-middle">
                  <i class="fa fa-whatsapp"></i>
                </div>
                <div class="media-body">
                  <div class="pull-left">
                    <h3 class="media-heading">
                      <?php Util::imprime($row[ddd2]); ?> <?php Util::imprime($row[telefone2]); ?>
                    </h3>
                  </div>
                  <div class="pull-right">
                    <a class="btn btn_telefone" href="tel:+55<?php Util::imprime($row[ddd2]); ?> <?php Util::imprime($row[telefone2]); ?>">
                      CHAMAR
                    </a>
                  </div>

                </div>
              </div>
            <?php endif; ?>
            <!-- ======================================================================= -->
            <!-- telefones  -->
            <!-- ======================================================================= -->

            <!-- ======================================================================= -->
            <!-- endereco  -->
            <!-- ======================================================================= -->
            <div class="media top20">
              <div class="media-left media-middle">
                <amp-img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_map.png" height="24" width="24" alt=""></amp-img>
              </div>
              <div class="media-body">
                <h3 class="media-heading">
                  <span><?php Util::imprime($row[endereco]); ?>
                  </span>
                </h3>
              </div>
            </div>
            <!-- ======================================================================= -->
            <!-- endereco  -->
            <!-- ======================================================================= -->

            <!-- ======================================================================= -->
            <!-- entrega  -->
            <!-- ======================================================================= -->
            <div class="media horario top20 pb10">
              <div class="media-left media-middle">
                <amp-img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_agenda.png" height="24" width="24" alt=""></amp-img>
              </div>
              <div class="media-body">
                <h3 class="media-heading">Horário de Funcionamento
                  <span>: <br> <?php Util::imprime($lojas[horario_abertura], 5); ?> às <?php Util::imprime($lojas[horario_fechamento], 5); ?>
                  </span>
                </h3>
              </div>
            </div>
            <!-- ======================================================================= -->
            <!-- entrega  -->
            <!-- ======================================================================= -->


            <div class="top15">
              <a class="btn btn_telefone input100" href="<?php echo Util::caminho_projeto() ?>/mobile/loja/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                SAIBA MAIS
              </a>
            </div>


          </div>
        </div>





        <?php
        if ($i==0) {
          echo "<div class='clearfix'>  </div>";
        }else {
          $i++;
        }
      }
    }
    ?>
  </div>

  <!-- ======================================================================= -->
  <!-- nossas lojas  -->
  <!-- ======================================================================= -->

  <div class="top50">  </div>
  <?php require_once("../includes/rodape.php") ?>




</body>



</html>
