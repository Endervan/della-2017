<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>


  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>
  form.amp-form-submit-success [submit-success],
  form.amp-form-submit-error [submit-error]{
    margin-top: 16px;
  }
  form.amp-form-submit-success [submit-success] {
    color: green;
  }
  form.amp-form-submit-error [submit-error] {
    color: red;
  }
  form.amp-form-submit-success.hide-inputs > input {
    display: none;
  }


  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 17); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>


  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>

</head>





<body class="bg-interna">


  <?php
  $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
  require_once("../includes/topo.php")
  ?>


  <div class="row">
    <!-- ======================================================================= -->
    <!--  titulo geral -->
    <!-- ======================================================================= -->
    <div class="col-12 localizacao-pagina titulo_emp text-center">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_personalizada.png" height="15" width="214" alt=""></amp-img>
      <div class="">
        <h2>ENTRE EM CONTATO</h2>
        <h1 class="text-capitalize">Fale Conosco</h1>
      </div>
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_personalizada.png" height="15" width="214" alt=""></amp-img>
    </div>
    <!-- ======================================================================= -->
    <!--  titulo -->
    <!-- ======================================================================= -->
  </div>

  <div class="row ">
    <!-- ======================================================================= -->
    <!-- CONTATOS  -->
    <!-- ======================================================================= -->
    <div class="col-6 ">
      <div class="menu_contatos input100 text-center">
        <a>
          <div class="media">
            <div class="media-left media-middle">
              <amp-img  class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_contato.png" height="15" width="19" alt=""></amp-img>
            </div>
            <div class="media-body">
              <h4 class="media-heading">FALE CONOSCO</h4>
            </div>
          </div>
        </a>
      </div>
    </div>

    <div class="col-6">
      <div class="menu_trabalhe input100 text-center">
        <a class="active" href="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco">
          <div class="media">
            <div class="media-left media-middle">
              <amp-img  class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_trabalhe.png" height="15" width="19" alt=""></amp-img>
            </div>
            <div class="media-body">
              <h4 class="media-heading">TRABALHE CONOSCO</h4>
            </div>
          </div>

        </a>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- CONTATOS  -->
    <!-- ======================================================================= -->
  </div>






  <div class="row bottom50">

    <div class="col-12">


      <?php
      //  VERIFICO SE E PARA ENVIAR O EMAIL
      if(isset($_GET[nome])){
        $texto_mensagem = "
        Unidade Desejada : ".($_GET[select_categorias])." <br />
        Nome: ".($_GET[nome])." <br />
        Email: ".($_GET[email])." <br />
        Telefone: ".($_GET[telefone])." <br />
        Assunto: ".($_GET[assunto])." <br />
        Fala com: ".($_GET[fala])." <br />

        Mensagem: <br />
        ".(nl2br($_GET[mensagem]))."
        ";

        Util::envia_email($config[email_fale_conosco], ("$_GET[nome] solicitou contato pelo site"), $texto_mensagem, ($_GET[nome]), $_GET[email]);
        Util::envia_email($config[email_copia], ("$_GET[nome] solicitou contato pelo site"), $texto_mensagem, ($_GET[nome]), $_GET[email]);
        $enviado = 'sim';
  //    unset($_GET);
      }
      ?>


      <?php if($enviado == 'sim'): ?>
          <div class="col-12  text-center top40">
              <h1>Mensagem enviado com sucesso.</h1>
          </div>
      <?php else: ?>

      
      <form method="get" action="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco/" >


        <div class="">
          <h5>ENVIE SEU EMAIL</h5>
        </div>

        <div class="ampstart-input inline-block relative form_contatos m0 p0 mb3 styled-select">



          <select name="select_categorias" class="input-form input100 block border-none p0 m0">

            <optgroup label="SELECIONE">
              <option value="" selected disabled hidden>SELECIONE A UNIDADE DESEJADA</option>
              <?php
              $result = $obj_site->select("tb_lojas");
              if (mysql_num_rows($result) > 0) {
                while($row = mysql_fetch_array($result)){
                  ?>
                  <option value="<?php Util::imprime($row[titulo]); ?>">
                    <?php Util::imprime($row[titulo]); ?>
                  </option>
                  <?php
                }
              }
              ?>
            </optgroup>
          </select>
        </div>

        <div class="clearfix">  </div>



        <div class="ampstart-input inline-block relative form_contatos m0 p0 mb3">
          <div class="relativo">
            <input type="text" class="input-form input100 block border-none p0 m0" name="nome" placeholder="NOME" required>
            <span class="fa fa-user form-control-feedback"></span>
          </div>

          <div class="relativo">
            <input type="email" class="input-form input100 block border-none p0 m0" name="email" placeholder="EMAIL" required>
            <span class="fa fa-envelope form-control-feedback"></span>
          </div>

          <div class="relativo">
            <input type="tel" class="input-form input100 block border-none p0 m0" name="telefone" placeholder="TELEFONE" required>
            <span class="fa fa-phone form-control-feedback"></span>
          </div>

          <div class="relativo">
            <input type="text" class="input-form input100 block border-none p0 m0" name="assunto" placeholder="ASSUNTO" required>
            <span class="fa fa-star form-control-feedback"></span>
          </div>

          <div class="relativo">
            <input type="text" class="input-form input100 block border-none p0 m0" name="fala" placeholder="FALAR COM" required>
            <span class="fa fa-users form-control-feedback"></span>
          </div>

          <div class="relativo">
            <textarea name="mensagem" placeholder="MENSAGEM" class="input-form input100 campo-textarea" ></textarea>
            <span class="fa fa-pencil form-control-feedback"></span>
          </div>

        </div>

        <div class="text-center">
          <input type="submit" value="ENVIAR" class="btn btn_formulario_fale">
        </div>


        <div submit-success>
          <template type="amp-mustache">
            Email enviado com sucesso! {{name}} entraremos em contato o mais breve possível.
          </template>
        </div>

        <div submit-error>
          <template type="amp-mustache">
            Houve um erro, {{name}} por favor tente novamente.
          </template>
        </div>


      </form>

      <?php endif; ?>


    </div>

  </div>


  <?php require_once("../includes/rodape.php") ?>

</body>



</html>
