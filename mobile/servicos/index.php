<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo",11);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 20); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>


</head>





<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <div class="row">
    <!-- ======================================================================= -->
    <!--  titulo geral -->
    <!-- ======================================================================= -->
    <div class="col-12 localizacao-pagina titulo_emp text-center">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_personalizada.png" height="15" width="214" alt=""></amp-img>
      <div class="">
        <h2>CONHEÇA NOSSOS</h2>
        <h1 class="text-capitalize">SERVIÇOS</h1>
      </div>
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_personalizada.png" height="15" width="214" alt=""></amp-img>
    </div>
    <!-- ======================================================================= -->
    <!--  titulo -->
    <!-- ======================================================================= -->
  </div>



  <!-- ======================================================================= -->
  <!-- CARDAPIO    -->
  <!-- ======================================================================= -->
  <div class="row cardapio_home bottom50">

    <ul >
      <?php
      $i=0;
      $result = $obj_site->select("tb_servicos");
      if (mysql_num_rows($result) > 0) {

        while ($row = mysql_fetch_array($result)) {
          ?>

          <li class="col-12 ">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
              <amp-img
              src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
              layout="responsive"
              width="400"
              height="500"
              alt="<?php echo Util::imprime($row[titulo]) ?>">
            </a>
          </li>
          <?php
        }

      }
      ?>

    </ul>
  </div>
  <!-- ======================================================================= -->
  <!-- CARDAPIO    -->
  <!-- ======================================================================= -->





  <?php require_once("../includes/rodape.php") ?>

</body>



</html>
