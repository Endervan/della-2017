<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>


  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>
  form.amp-form-submit-success [submit-success],
  form.amp-form-submit-error [submit-error]{
    margin-top: 16px;
  }
  form.amp-form-submit-success [submit-success] {
    color: green;
  }
  form.amp-form-submit-error [submit-error] {
    color: red;
  }
  form.amp-form-submit-success.hide-inputs > input {
    display: none;
  }


  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 17); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>


  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>

</head>





<body class="bg-interna">


  <?php
  $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
  require_once("../includes/topo.php")
  ?>


  <div class="row">
    <!-- ======================================================================= -->
    <!--  titulo geral -->
    <!-- ======================================================================= -->
    <div class="col-12 localizacao-pagina titulo_emp text-center">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_personalizada.png" height="15" width="214" alt=""></amp-img>
      <div class="">
        <h2>FAÇA PARTE DA NOSSA EQUIPE</h2>
        <h1 class="text-capitalize">Trablhe Conosco</h1>
      </div>
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_personalizada.png" height="15" width="214" alt=""></amp-img>
    </div>
    <!-- ======================================================================= -->
    <!--  titulo -->
    <!-- ======================================================================= -->
  </div>

  <div class="row ">
    <!-- ======================================================================= -->
    <!-- CONTATOS  -->
    <!-- ======================================================================= -->
    <div class="col-6 ">
      <div class=" menu_trabalhe input100 pg15 text-center">
        <a class="active" href="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco">
          <div class="media">
            <div class="media-left media-middle">
              <amp-img  class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_contato.png" height="15" width="19" alt=""></amp-img>
            </div>
            <div class="media-body">
              <h4 class="media-heading">FALE CONOSCO</h4>
            </div>
          </div>
        </a>
      </div>
    </div>

    <div class="col-6">
      <div class="menu_contatos  input100 pg15 text-center">
        <a >
          <div class="media">
            <div class="media-left media-middle">
              <amp-img  class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_trabalhe.png" height="15" width="19" alt=""></amp-img>
            </div>
            <div class="media-body">
              <h4 class="media-heading">TRABALHE CONOSCO</h4>
            </div>
          </div>

        </a>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- CONTATOS  -->
    <!-- ======================================================================= -->
  </div>






  <div class="row bottom50">

    <div class="col-12">

      <?php
      //  VERIFICO SE E PARA ENVIAR O EMAIL
      if(isset($_GET[nome])){
        $texto_mensagem = "
        Nome: ".($_GET[nome])." <br />
        Email: ".($_GET[email])." <br />
        Sexo: ".($_GET[sexo])." <br />
        Nascimento: ".($_GET[nascimento])." <br />
        Grau de Instrução: ".($_GET[grau_instrucao])." <br />
        Endereco: ".($_GET[endereco])." <br />
        Area Predentido: ".($_GET[area_predentido])." <br />
        Cargo Predentido: ".($_GET[cargo_predentido])." <br />
        Curricúlo: ".($_GET[curriculo])." <br />

        Mensagem: <br />
        ".(nl2br($_GET[mensagem]))."
        ";


        Util::envia_email($config[email_fale_conosco], utf8_decode("$_GET[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_GET[nome]), $_GET[email]);
        Util::envia_email($config[email_copia], utf8_decode("$_GET[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_GET[nome]), $_GET[email]);

        Util::alert_bootstrap("Obrigado por entrar em contato.");
        unset($_GET);
      }
      ?>


      <div class="">
        <h5>ENVIE SEUS DADOS PROFISSIONAIS</h5>
      </div>
      <form method="get" action="envia.php" target="_top">


        <div class="ampstart-input inline-block relative m0 p0 mb3 trabalhe_conosco">

          <div class="relativo">
            <div class="">NOME:</div>
            <input type="text" class="input-form input100 block border-none p0 m0" name="nome"  required>
            <span class="fa fa-user form-control-feedback"></span>
          </div>

          <div class="relativo">
            <div class="">EMAIL:</div>
            <input type="email" class="input-form input100 block border-none p0 m0" name="email"  required>
            <span class="fa fa-envelope form-control-feedback"></span>
          </div>

          <div class="relativo">
            <div class="">TELEFONE</div>
            <input type="tel" class="input-form input100 block border-none p0 m0" name="telefone"  required>
            <span class="fa fa-phone form-control-feedback"></span>
          </div>

          <div class="relativo">
            <div class="">SEXO</div>
            <input type="text" class="input-form input100 block border-none p0 m0" name="sexo"  required>
            <span class="fa fa-star form-control-feedback"></span>
          </div>

          <div class="relativo">
            <div class="">NASCIMENTO</div>
            <input class="input-form input100 block border-none p0 m0" name="nascimento"  type="date" required>
            <span class="fa fa-calendar form-control-feedback"></span>
          </div>

          <div class="relativo">
            <div>GRAU DE INSTRUÇÃO</div>
            <input type="text" class="input-form input100 block border-none p0 m0" name="grau_instrucao" required>
            <span class="fa fa-list-alt form-control-feedback"></span>
          </div>

          <div class="relativo">
            <div>ENDEREÇO</div>
            <input type="text" class="input-form input100 block border-none p0 m0" name="endereco" required>
            <span class="fa fa-map-marker form-control-feedback"></span>
          </div>

          <div class="relativo">
            <div>ÁREA PRETENDIDO</div>
            <input type="text" class="input-form input100 block border-none p0 m0" name="area_predentido" required>
            <span class="fa fa-folder-open form-control-feedback"></span>
          </div>

          <div class="relativo">
            <div>CARGO PRETENDIDO</div>
            <input type="text" class="input-form input100 block border-none p0 m0" name="cargo_predentido" placeholder="" required>
            <span class="fa fa-folder-open form-control-feedback"></span>
          </div>

          <div class="relativo curriculo">
            <div>CURRÍCULO</div>
            <input type="file" class="input-form input100 block border-none p0 m0 pt20 curriculo" name="curriculo" required>
            <span class="fa fa-file-text form-control-feedback"></span>
          </div>


          <div class="relativo">
            <div>MENSAGEM</div>
            <textarea name="mensagem" class="input-form input100 campo-textarea" ></textarea>
            <span class="fa fa-pencil form-control-feedback"></span>
          </div>

        </div>

        <div class="text-center">
          <input type="submit" value="ENVIAR" class="ampstart-btn caps btn btn_formulario_fale">
        </div>

        <div submit-success>
          <template type="amp-mustache">
            Email enviado com sucesso! {{name}} entraremos em contato o mais breve possível.
          </template>
        </div>

        <div submit-error>
          <template type="amp-mustache">
            Houve um erro, {{name}} por favor tente novamente.
          </template>
        </div>


      </form>
    </div>

    </div>


    <?php require_once("../includes/rodape.php") ?>

    </body>



    </html>
