
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
$obj_data = new Data_Hora();

// INTERNA
$url =$_GET[get1];

if(!empty($url)){
	$complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0){
	Util::script_location(Util::caminho_projeto()."/mobile/produtos/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!doctype html>
<html amp lang="pt-br">
<head>

	<!--  ==============================================================  -->
	<!-- SELECT BOOSTRAP-->
	<!--  ==============================================================  -->
	<script src="<?php echo Util::caminho_projeto() ?>/js/jquery-2.1.4.min.js"></script>


	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

	<!-- (Optional) Latest compiled and minified JavaScript translation files -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>




	<?php require_once("../includes/head.php"); ?>
	<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

	<style amp-custom>
	<?php require_once("../css/geral.css"); ?>
	<?php require_once("../css/topo_rodape.css"); ?>
	<?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



	<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 14); ?>
	.bg-interna{
		background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
	}
	</style>


	<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	<script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
	<script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>



	<script type="text/javascript">
	$('.selectpicker').selectpicker({
		style: 'btn-info',
		size: 8
	});

	</script>
	<!--  ==============================================================  -->
	<!-- SELECT BOOSTRAP-->
	<!--  ==============================================================  -->




</head>





<body class="bg-interna pagina_produto">


	<?php
	$voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
	require_once("../includes/topo.php")
	?>


	<div class="row">
		<div class="col-12 localizacao-pagina text-center">
			<h1><?php echo Util::imprime($dados_dentro[titulo]) ?></h1>
			<amp-img  src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/borda_titulo.png" alt="Home" height="5" width="90"></amp-img>
		</div>
	</div>



	<div class="row produtos_light">
		<div class="col-12 top10">

			<amp-carousel id="carousel-with-preview"
			width="450"
			height="314"
			layout="responsive"
			type="slides"
			autoplay
			delay="4000"
			>

			<?php
			$i = 0;
			$result = $obj_site->select("tb_galerias_produtos", "and id_produto = $dados_dentro[idproduto]");
			if (mysql_num_rows($result) > 0) {
				while($row = mysql_fetch_array($result)){
					?>


					<amp-img
					on="tap:lightbox1"
					role="button"
					tabindex="0"

					src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
					layout="responsive"
					width="450"
					height="314"
					alt="<?php echo Util::imprime($row[titulo]) ?>">

				</amp-img>
				<?php
			}
			$i++;
		}
		?>
	</amp-carousel>

	<amp-image-lightbox id="lightbox1"layout="nodisplay">	</amp-image-lightbox>




	<div class="carousel-preview text-center">

		<?php
		$i = 0;
		$result = $obj_site->select("tb_galerias_produtos", "and id_produto = $dados_dentro[idproduto]");
		if (mysql_num_rows($result) > 0) {
			while($row = mysql_fetch_array($result)){
				?>

				<button on="tap:carousel-with-preview.goToSlide(index=<?php echo $i++; ?>)">

					<amp-img
					src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/bolinhas.png"
					width="21"
					height="21"
					layout="responsive"
					alt="<?php echo Util::imprime($row[titulo]) ?>">
				</amp-img>
			</button>

			<?php
		}
		$i++;
	}
	?>
</div>


<div class="row ">
	<div class="col-12 top20">
		<div class="col-12">
			<amp-img
			on="tap:my-lightbox"
			role="button"
			src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/btn_atentimento.png"
			width="400"
			height="58"
			alt=""
			layout="responsive"
			tabindex="0">
		</amp-img>
	</div>
</div>
</div>


<amp-lightbox
id="my-lightbox"
layout="nodisplay">


<div class="lightbox_produto lightbox"role="button" tabindex="0">

	<div>
		<div class="col-12"><button class="btn btn_fechar" on="tap:my-lightbox.close">Fechar</button></div>
		<div class="col-12 fundo_lojas_produto text-center">	<h6>Selecione a Loja Desejada</h6></div>
		<!-- ======================================================================= -->
		<!-- ATENTIMENTO LOJAS -->
		<!-- ======================================================================= -->
		<?php
		$result = $obj_site->select("tb_lojas");
		if(mysql_num_rows($result) > 0){
			while($row = mysql_fetch_array($result)){
				$i=0;
				?>

				<div class="col-12 bottom20 fundo_lojas_produto">
					<!-- ======================================================================= -->
					<!-- telefones  -->
					<!-- ======================================================================= -->
					<div class="col-12">
						<div class="media">
							<div class="col-5 padding0 pt10">
								<h2 ><?php Util::imprime($row[titulo]); ?></h2>
							</div>

							<div class="col-7 padding0">
								<div class="media-left media-middle">
									<amp-img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_telefone1.png" height="15" width="15" alt=""></amp-img>
								</div>
								<div class="media-body">
									<div class="pull-left">
										<h3 class="media-heading top15">
											<?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?>
										</h3>
									</div>
									<div class="pull-right">
										<a class="btn btn_telefone2" href="tel:+55<?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?>">
											CHAMAR
										</a>
									</div>
								</div>
							</div>

						</div>



						<?php if (!empty($row[telefone2])): ?>
							<div class="media">
								<div class="col-7 col-offset-5 padding0">
									<div class="media-left media-middle">
										<i class="fa fa-whatsapp"></i>
									</div>
									<div class="media-body">
										<div class="pull-left">
											<h3 class="media-heading top15">
												<?php Util::imprime($row[ddd2]); ?> <?php Util::imprime($row[telefone2]); ?>
											</h3>
										</div>
										<div class="pull-right">
											<a class="btn btn_telefone2" href="tel:+55<?php Util::imprime($row[ddd2]); ?> <?php Util::imprime($row[telefone2]); ?>">
												CHAMAR
											</a>
										</div>
									</div>
								</div>
							</div>
						<?php endif; ?>



					</div>
					<!-- ======================================================================= -->
					<!-- telefones  -->
					<!-- ======================================================================= -->
				</div>

				<?php
				if ($i==0) {
					echo "<div class='clearfix'>  </div>";
				}else {
					$i++;
				}
			}
		}
		?>
	</div>
	<!-- ======================================================================= -->
	<!-- ATENTIMENTO LOJAS -->
	<!-- ======================================================================= -->
</div>
</amp-lightbox>



</div>
</div>


<?php /*
<div class="row">
	<div class="col-12 desc_prod_opcoes_compra">
		<div class="top20">
			<?php require_once("../includes/opcoes_produtos.php"); ?>
		</div>
	</div>
</div>
*/ ?>





<div class="row">
	<div class="col-12 decricao_produto">

		<?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 6);?>
		<div class="top20 desc_prod">
			<p><?php Util::imprime($row[descricao]); ?></p>
		</div>
		<div class="clearfix"></div>

		<div class="top50 text-center">
			<h1>DETALHES DO PRODUTO</h1>
			<amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_empresa_home.png" height="3" width="100" alt=""></amp-img>
		</div>
		<div class="top20"><p ><?php echo Util::imprime($dados_dentro[descricao]) ?></p></div>
	</div>
</div>



<!-- ======================================================================= -->
<!-- PRODUTOS    -->
<!-- ======================================================================= -->
<div class="row bottom50">

	<div class="col-12 top30 decricao_produto">
		<h1>VEJA TAMBÉM</h1>
		<amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_empresa_home.png" height="3" width="100" alt=""></amp-img>
	</div>
	<?php
	$result = $obj_site->select("tb_produtos","order by rand() limit 2");
	if(mysql_num_rows($result) > 0){
		while($row = mysql_fetch_array($result)){
			$i=0;
			?>

			<div class="col-6  produtos_home">
				<div class="thumbnail">
					<a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>">
						<amp-img layout="responsive" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php echo Util::imprime($row[titulo]) ?>" height="158" width="210"></amp-img>
					</a>
					<div class="caption">
						<div class="desc_titulo"><h1><?php Util::imprime($row[titulo]); ?></h1></div>
						<div class="">
							<a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn_saiba_mais" role="button">SAIBA MAIS <i class="fa fa-plus-circle left10"></i></a>
						</div>
					</div>
					<a class="btn btn_produtos_orcamento" href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn_prod_solicitar_orcamento" title="EFETUAR PEDIDO">
						<i class="fa fa-shopping-cart right5"></i>
						EFETUAR PEDIDO
					</a>
				</div>
			</div>

			<?php
			if($i == 1){
				echo '<div class="clearfix"></div>';
				$i = 0;
			}else{
				$i++;
			}

		}
	}
	?>

</div>
<!-- ======================================================================= -->
<!-- PRODUTOS    -->
<!-- ======================================================================= -->



<?php require_once("../includes/rodape.php") ?>

</body>



</html>




<!-- Bootstrap Validator
https://github.com/nghuuphuoc/bootstrapvalidator
================================================== -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/bootstrapValidator.min.css"/>


<!-- Either use the compressed version (recommended in the production site) -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/bootstrapValidator.min.js"></script>

<!-- Or use the original one with all validators included -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/bootstrapValidator.js"></script>

<!-- language -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/src/js/language/pt_BR.js"></script>


<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/bootstrap-lightbox.min.css">
<script src="<?php echo Util::caminho_projeto() ?>/js/bootstrap-lightbox.min.js"></script>


<script>
$(function () {
	$('[data-toggle="tooltip"]').tooltip()
})
</script>


<script>
$(document).ready(function() {
	$('.FormPedido').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {


			preco: {
				validators: {
					notEmpty: {
						message: 'Por favor, selecione uma opção.'
					}
				}
			},

			<?php if($dados_dentro[obrigatorio_quantidade] == 'SIM'): ?>
			quantidade: {
				validators: {
					notEmpty: {
						message: 'Por favor, selecione a quantidade desejada.'
					}
				}
			},
		<?php endif; ?>

			<?php echo $validacao_javascript; ?>

		}
	});
});
</script>
