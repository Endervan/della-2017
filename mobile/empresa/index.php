<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>


  <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>


</head>





<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <div class="row">

    <!-- ======================================================================= -->
    <!--  titulo geral -->
    <!-- ======================================================================= -->
    <div class="col-12 localizacao-pagina titulo_emp text-center">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_personalizada.png" height="15" width="214" alt=""></amp-img>
      <div class="">
        <h2>SAIBA MAIS SOBRE A </h2>
        <h1 class="text-capitalize">Della Empório</h1>
      </div>
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_personalizada.png" height="15" width="214" alt=""></amp-img>
    </div>
    <!-- ======================================================================= -->
    <!--  titulo -->
    <!-- ======================================================================= -->

    <div class="col-12 desc-empresa">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 8);?>
      <p><?php Util::imprime($row[descricao]); ?></p>
    </div>

  </div>



  <div class="row">
    <div class="col-12 top10 clientes_emp bottom50">


      <amp-carousel id="carousel-with-preview"
      width="450"
      height="314"
      layout="responsive"
      type="slides"
      autoplay
      delay="4000">

      <?php
      $i = 0;
      $result = $obj_site->select("tb_banners","AND tipo_banner = 3");
      if (mysql_num_rows($result) > 0) {
        while($row = mysql_fetch_array($result)){
          ?>


          <amp-img
          src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
          width="450"
          height="374"
          layout="responsive"
          alt="<?php echo Util::imprime($row[titulo]) ?>">
        </amp-img>
        <?php
      }
      $i++;
    }
    ?>
  </amp-carousel>



  <div class="carousel-preview text-center">

    <?php
    $i = 0;
    $result = $obj_site->select("tb_banners","AND tipo_banner = 3");
    if (mysql_num_rows($result) > 0) {
      while($row = mysql_fetch_array($result)){
        ?>

        <button on="tap:carousel-with-preview.goToSlide(index=<?php echo $i++; ?>)">

          <amp-img
          src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/bolinhas.png"
          width="21"
          height="21"
          layout="responsive"
          alt="<?php echo Util::imprime($row[titulo]) ?>">
        </amp-img>
      </button>

      <?php
    }
    $i++;
  }
  ?>

</div>



</div>
</div>

<div class="row bg_onde">
  <div class="col-12 top135 text-center">



    <button class="ampstart-btn caps m2 btn btn_lojas"
    on="tap:my-lightbox"
    role="button"
    tabindex="0">
    CONFIRA NOSSAS LOJAS
  </button>




</div>
</div>

<amp-lightbox
id="my-lightbox"
layout="nodisplay">
<div class="lightbox"

role="button"
tabindex="0">

<!-- ======================================================================= -->
<!-- nossas lojas  -->
<!-- ======================================================================= -->
<div class="row">
  <div class="col-12">  <button class="btn btn_fechar" on="tap:my-lightbox.close">Fechar</button></div>
  <?php
  $result = $obj_site->select("tb_lojas");
  if(mysql_num_rows($result) > 0){
    while($row = mysql_fetch_array($result)){
      $i=0;
      ?>

      <div class="col-12 ">
        <div class="titulo_loja">

          <div class=" text-center pt15">
            <h2 class="text-uppercase"><?php Util::imprime($row[titulo]); ?></h2>
          </div>

          <div class="clearfix"></div>

          <div class="top15">
            <a class="btn btn_telefone1 input100" href="<?php echo Util::caminho_projeto() ?>//mobile/loja/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
              SAIBA MAIS
            </a>
          </div>


        </div>
      </div>





      <?php
      if ($i==0) {
        echo "<div class='clearfix'>  </div>";
      }else {
        $i++;
      }
    }
  }
  ?>
</div>

<!-- ======================================================================= -->
<!-- nossas lojas  -->
<!-- ======================================================================= -->



</div>
</amp-lightbox>



<?php require_once("../includes/rodape.php") ?>

</body>



</html>
