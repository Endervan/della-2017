<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 15); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>


  <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
  <script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>


</head>





<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <div class="row">
    <!-- ======================================================================= -->
    <!--  titulo geral -->
    <!-- ======================================================================= -->
    <div class="col-12 localizacao-pagina titulo_emp text-center">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_personalizada.png" height="15" width="214" alt=""></amp-img>
      <div class="">
        <h2>ANUNCIE EM NOSSA </h2>
        <h1 class="text-capitalize">Tv Corporativa</h1>
      </div>
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_personalizada.png" height="15" width="214" alt=""></amp-img>
    </div>
    <!-- ======================================================================= -->
    <!--  titulo -->
    <!-- ======================================================================= -->
  </div>



  <div class="row empresa">

    <!-- ======================================================================= -->
    <!-- conheca nossa empresa  -->
    <!-- ======================================================================= -->
    <div class="col-12">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5);?>

      <div class="desc_empresa">
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>

    </div>
    <!-- ======================================================================= -->
    <!-- conheca nossa empresa  -->
    <!-- ======================================================================= -->


    <!-- ======================================================================= -->
    <!-- COMO ANUNCIAR  -->
    <!-- ======================================================================= -->
    <div class="col-12 ">
      <div class="text-center">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>
        <div class="">
          <h1><?php Util::imprime($row[titulo]); ?></h1>
          <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_empresa_home.png" height="3" width="100" alt=""></amp-img>
        </div>

        <div class="top25">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>

      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- COMO ANUNCIAR  -->
    <!-- ======================================================================= -->

    <!-- ======================================================================= -->
    <!-- porque ANUNCIAR  -->
    <!-- ======================================================================= -->
    <div class="col-12 bottom50">
      <div class="bg_amarelo pg10 text-center">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4);?>
        <div class="top90">
          <h1><?php Util::imprime($row[titulo]); ?></h1>
          <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_empresa_home.png" height="3" width="100" alt=""></amp-img>
        </div>

        <div class="top25 pb40">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>

      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- porque ANUNCIAR  -->
    <!-- ======================================================================= -->




  </div>









  <?php require_once("../includes/rodape.php") ?>

</body>



</html>
