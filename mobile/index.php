
<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("./includes/head.php"); ?>


  <style amp-custom>
  <?php require_once("./css/geral.css"); ?>
  <?php require_once("./css/topo_rodape.css"); ?>
  <?php require_once("./css/paginas.css");  //  ARQUIVO DA PAGINA ?>
  </style>
</head>



<body class="bg-index">


  <div class="row">
    <div class="col-12 text-center topo">
      <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home" height="100" width="198"></amp-img>
    </div>
  </div>


  <div class="row font-index">

    <div class="col-12 bottom10">
      <div class="text-center">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 7);?>
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>
    </div>


    <div class="col-4 text-center">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa">
        <div class="index-bg-icones">
          <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_empresa_home.png" alt="A EMPRESA" height="35" width="42"></amp-img>
          <h1>A DELLA</h1>
        </div>

      </a>
    </div>

    <div class="col-4 text-center">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">
        <div class="index-bg-icones">
          <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_produtos_home.png" alt="A EMPRESA" height="35" width="42"></amp-img>
          <h1>PRODUTOS</h1>
        </div>

      </a>
    </div>

    <div class="col-4 text-center">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/lojas">
        <div class="index-bg-icones">
          <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_lojas_home.png" alt="A EMPRESA" height="35" width="42"></amp-img>
          <h1>NOSSAS LOJAS</h1>
        </div>

      </a>
    </div>

    <div class="clearfix">  </div>

    <div class="col-2">  </div>





    <div class="col-4 text-center">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco">
        <div class="index-bg-icones">
          <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_fale_home.png" alt="FALE CONOSCO" height="35" width="42"></amp-img>
          <h1>FALE CONOSCO</h1>
        </div>

      </a>
    </div>

    <div class="col-4 text-center">

      <div class="index-bg-icones">
        <a
        on="tap:my-lightbox002"
        role="a"
        tabindex="0">
        <i class="fa fa-map-marker" aria-hidden="true" style="color: #643e36; font-size: 2.5em;"></i>
        <h1>ONDE ESTAMOS</h1>

      </a>
    </div>


  </div>



  <div class="rodape_pai">

    <amp-lightbox
    id="my-lightbox002"
    layout="nodisplay">

    <div class="lightbox"
    role="a"
    tabindex="0"
    on="tap:my-lightbox002.close">

    <!-- ======================================================================= -->
    <!-- nossas lojas  -->
    <!-- ======================================================================= -->
    <div class=" ">
      <div class="">
        <div class="col-12">  <button class="btn btn_fechar" on="tap:my-lightbox002.close">Fechar</button></div>
        <?php
        $result = $obj_site->select("tb_lojas");
        if(mysql_num_rows($result) > 0){
          while($row = mysql_fetch_array($result)){
            $i=0;
            ?>

            <div class="col-12">
              <div class="titulo_loja">

                <div class=" text-center pt5">
                  <h2 class="text-uppercase"><?php Util::imprime($row[titulo]); ?></h2>
                </div>

                <div class="col-6 col-offset-3 text-center">
                  <a class="btn btn_telefone input100" href="<?php Util::imprime($row[link_maps]); ?>" target="_blank">
                    ABRIR MAPA
                  </a>
                </div>

              </div>
            </div>





            <?php
            if ($i==0) {
              echo "<div class='clearfix'>  </div>";
            }else {
              $i++;
            }
          }
        }
        ?>
      </div>
    </div>

    <!-- ======================================================================= -->
    <!-- nossas lojas  -->
    <!-- ======================================================================= -->



  </div>
</amp-lightbox>
</div>





</div>


<?php require_once("./includes/rodape.php") ?>




</body>



</html>
