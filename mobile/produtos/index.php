<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 13); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>

  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>

</head>





<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <div class="row">
    <!-- ======================================================================= -->
    <!--  titulo geral -->
    <!-- ======================================================================= -->
    <div class="col-12 localizacao-pagina titulo_emp text-center">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_personalizada.png" height="15" width="214" alt=""></amp-img>
      <div class="">
        <h2>CONHEÇA NOSSOS</h2>
        <h1 class="text-capitalize">Produtos</h1>
      </div>
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_personalizada.png" height="15" width="214" alt=""></amp-img>
    </div>
    <!-- ======================================================================= -->
    <!--  titulo -->
    <!-- ======================================================================= -->
  </div>



  <div class="row ">

    <div class="col-12 top10">
      <amp-img class=""
      on="tap:my-lightbox"
      role="button"
      src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/categorias.png"
      height="55"
      width="400"
      alt=""
      layout="responsive"
      tabindex="0">
    </amp-img>
  </div>

</div>



<amp-lightbox
id="my-lightbox"
layout="nodisplay">
<div class="lightbox"

role="button"
tabindex="0">

<!-- ======================================================================= -->
<!-- nossas lojas  -->
<!-- ======================================================================= -->
<div class="row">
  <div class="col-12">  <button class="btn btn_fechar" on="tap:my-lightbox.close">Fechar</button></div>

  <!-- ======================================================================= -->
  <!-- CATEGORIAS HOME -->
  <!-- ======================================================================= -->

  <div class="col-12 lato_black text-center lista_categorias">
    <div id="slider" class="flexslider">
      <ul class="slides">

        <?php
        $i=0;
        $result = $obj_site->select("tb_categorias_produtos");
        if (mysql_num_rows($result) > 0) {

          while ($row = mysql_fetch_array($result)) {
            ?>
            <li class="col-6 active">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                <amp-img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" height="57" width="69"></amp-img>
                <h4><?php Util::imprime($row[titulo]); ?></h4>
              </a>
            </li>
            <?php
            if($i == 1){
              echo '<div class="clearfix"></div>';
              $i = 0;
            }else{
              $i++;
            }

          }
        }
        ?>

      </ul>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- CATEGORIAS HOME -->
  <!-- ======================================================================= -->




</div>

<!-- ======================================================================= -->
<!-- nossas lojas  -->
<!-- ======================================================================= -->
</div>
</amp-lightbox>


<!-- ======================================================================= -->
<!-- PRODUTOS    -->
<!-- ======================================================================= -->
<div class="row">

  <?php
  //  busca os produtos sem filtro
  $result = $obj_site->select("tb_produtos", $complemento);

  $url =$_GET[get1];

  //  FILTRA AS CATEGORIAS
  if (isset( $url )) {
    $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url);
    $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
  }

  $result = $obj_site->select("tb_produtos", $complemento);

  ?>

  <?php
  if(mysql_num_rows($result) == 0){
    echo "<p class='bg-danger clearfix' style='padding: 20px;'>Nenhum produto encontrado</p>";
  }else{
    ?>
    <div class="col-12 total-resultado-busca">
      <h1><span><?php echo mysql_num_rows($result) ?></span> PRODUTO(S) ENCONTRADO(S) . </h1>
    </div>

    <?php
    $i = 0;
    while($row = mysql_fetch_array($result))
    {
      $result1 = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
      $row1 = mysql_fetch_array($result1);
      ?>

      <div class="col-6  produtos_home">
        <div class="thumbnail">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>">
            <amp-img layout="responsive" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php echo Util::imprime($row[titulo]) ?>" height="158" width="210"></amp-img>
          </a>
          <div class="caption">
            <div class="desc_titulo text-uppercase"><h1><?php Util::imprime($row[titulo]); ?></h1></div>
            <div class="">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn_saiba_mais" role="button">SAIBA MAIS <i class="fa fa-plus-circle left10"></i></a>
            </div>
          </div>
          <a class="btn btn_produtos_orcamento" href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php echo $row[url_amigavel] ?>" class="btn btn_prod_solicitar_orcamento" title="EFETUAR PEDIDO">
            <i class="fa fa-shopping-cart right5"></i>
            EFETUAR PEDIDO
          </a>
        </div>
      </div>

      <?php
      if($i == 1){
        echo '<div class="clearfix"></div>';
        $i = 0;
      }else{
        $i++;
      }

    }
  }
  ?>

</div>
<!-- ======================================================================= -->
<!-- PRODUTOS    -->
<!-- ======================================================================= -->

<div class="top50">  </div>

<?php require_once("../includes/rodape.php") ?>

13

RS058208260BR


</body>



</html>
