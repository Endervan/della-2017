
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
$obj_data = new Data_Hora();

// INTERNA
$url =$_GET[get1];

if(!empty($url)){
	$complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_servicos", $complemento);

if(mysql_num_rows($result)==0){
	Util::script_location(Util::caminho_projeto()."/mobile/servicos/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!doctype html>
<html amp lang="pt-br">
<head>

	<?php require_once("../includes/head.php"); ?>
	<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

	<style amp-custom>
	<?php require_once("../css/geral.css"); ?>
	<?php require_once("../css/topo_rodape.css"); ?>
	<?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



	<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 14); ?>
	.bg-interna{
		background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
	}
	</style>


	<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	<script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
	<script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>


</head>


<body class="bg-interna">


	<?php
	$voltar_para = 'servicos'; // link de volta, exemplo produtos, dicas, servicos etc
	require_once("../includes/topo.php")
	?>


	<div class="row">
		<div class="col-12 localizacao-pagina titulo_emp text-center top15">
			<h1><?php echo Util::imprime($dados_dentro[titulo]) ?></h1>
			<amp-img  src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/borda_titulo.png" alt="Home" height="5" width="90"></amp-img>
		</div>
	</div>



	<div class="row produtos_light bottom50">
		<div class="col-12 top10">

			<amp-carousel id="carousel-with-preview"
			width="450"
			height="314"
			layout="responsive"
			type="slides"
			autoplay
			delay="4000"
			>

			<?php
			$i = 0;
			$result = $obj_site->select("tb_galerias_servicos", "and id_servico = $dados_dentro[idservico]");
			if (mysql_num_rows($result) > 0) {
				while($row = mysql_fetch_array($result)){
					?>
					<amp-img
					on="tap:lightbox1"
					role="button"
					tabindex="0"

					src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
					layout="responsive"
					width="450"
					height="314"
					alt="<?php echo Util::imprime($row[titulo]) ?>">

				</amp-img>
				<?php
			}
			$i++;
		}
		?>
	</amp-carousel>
	<amp-image-lightbox id="lightbox1"layout="nodisplay">	</amp-image-lightbox>
	<div class="carousel-preview text-center">
		<?php
		$i = 0;
		$result = $obj_site->select("tb_galerias_servicos", "and id_servico = $dados_dentro[idservico]");
		if (mysql_num_rows($result) > 0) {
			while($row = mysql_fetch_array($result)){
				?>

				<button on="tap:carousel-with-preview.goToSlide(index=<?php echo $i++; ?>)">

					<amp-img
					src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/bolinhas.png"
					width="21"
					height="21"
					layout="responsive"
					alt="<?php echo Util::imprime($row[titulo]) ?>">
				</amp-img>
			</button>

			<?php
		}
		$i++;
	}
	?>
</div>

<div class="top25">
	<p><?php Util::imprime($dados_dentro[descricao]); ?></p>
</div>

</div>
</div>



<?php require_once("../includes/rodape.php") ?>

</body>


</html>
