<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
$obj_data = new Data_Hora();
$obj_carrinho = new Carrinho();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];



/* ======================================================================= */
/* ADICIONA O PRODUTO AO CARRINHO */
/* ======================================================================= */
if(isset($_POST[add_item])){
  $obj_carrinho->add_item($_POST);
}




/* ======================================================================= */
/* EXCLUI O PRODUTO AO CARRINHO */
/* ======================================================================= */
if(isset($_GET[action])){
  if ($_GET[action] == 'excluir') {
    $obj_carrinho->del_item($_GET[id]);
  }

}



/* ======================================================================= */
/* VERIFICA SE O CARRINHO ESTA VAZIO */
/* ======================================================================= */
if(count($_SESSION[pedido]) == 0){
  $retorno = Util::caminho_projeto() . '/mobile/produtos';
  header("location: $retorno");
}

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>

  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>








  <script src="<?php echo Util::caminho_projeto() ?>/js/jquery-2.1.4.min.js"></script>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <!-- Bootstrap Validator
  https://github.com/nghuuphuoc/bootstrapvalidator
  ================================================== -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/bootstrapValidator.min.css"/>


  <!-- Either use the compressed version (recommended in the production site) -->
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/bootstrapValidator.min.js"></script>

  <!-- Or use the original one with all validators included -->
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/bootstrapValidator.js"></script>

  <!-- language -->
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/src/js/language/pt_BR.js"></script>


  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/bootstrap-lightbox.min.css">
  <script src="<?php echo Util::caminho_projeto() ?>/js/bootstrap-lightbox.min.js"></script>



  <script>
  $(document).ready(function(){


    //  forma de pagamento
    $(".troco-para").hide();


    $(".forma-pagamento").click(function(){

      if ( $("input[name='forma_pagamento']:checked").val() == 'dinheiro' ) {
        $(".troco-para").show();
      }else{
        $(".troco-para").hide();
      };
    });
    //  forma de pagamento



    $(".troca-cep").click(function(){
      $('#myModalcep').modal({
        backdrop: 'static',
        keyboard: false  // to prevent closing with Esc button (if you want this too)
      })
    });

  });
  </script>


</head>





<body class="bg-interna pagina_produto">


  <?php require_once("../includes/topo.php") ?>



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="row">
    <div class="col-12 localizacao-pagina titulo_emp text-center">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_personalizada.png" height="15" width="214" alt=""></amp-img>
      <div class="">
        <h2>SEU PEDIDO</h2>
        <h1 class="text-capitalize">Della Empório</h1>
      </div>
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_personalizada.png" height="15" width="214" alt=""></amp-img>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  modal cep -->
  <!-- ======================================================================= -->
  <div id="myModalcep" class="modal fade" style="z-index=9999">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title">Aviso</h1>
        </div>
        <div class="modal-body">

          <p>Selecione a forma de entrega desejada.</p>

<div class="top15">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation"><a href="#entrega-cep" class="btn btn-primary" aria-controls="entrega-cep" role="tab" data-toggle="tab">Receba Em casa</a></li>
    <li role="presentation"><a href="#entrega-retirada" class="btn btn-primary" aria-controls="entrega-retirada" role="tab" data-toggle="tab">Retirada na Loja</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="entrega-cep">
      <div class="col-xs-12"><p class="top15">Para calcularmos a taxa de entrega, por favor, informe seu CEP.</p></div>
      <form action="<?php echo Util::caminho_projeto() ?>/mobile/pedido/" class="top20 bottom29 FormCep" method="post" id="form_rodape">
        <div class="form-group">
          <div class="col-xs-8">
            <input type="text" name="session_cep_entrega" class="input-todas-bordas form-control fundo-form1 fundo-form cep-rodape"  >
          </div>
          <div class="col-xs-4 pt0">
          <input type="submit" name="btn_session_cep_entrega" value="Continuar" class="btn btn-primary pull-right">
          </div>
        </div>
      </form>
    </div>


    <div role="tabpanel" class="tab-pane" id="entrega-retirada">
    <div class="col-xs-12"><p class="top15">Selecione a loja deseja para retirar sua compra.</p></div>
      <form action="<?php echo Util::caminho_projeto() ?>/mobile/pedido/" class="top20 bottom29 FormRetiradaLoja" method="post" id="form_rodape1">
        <div class="form-group">
          <div class="col-xs-8">
            <?php Util::cria_select_bd("tb_lojas", "idloja", "titulo", "id_loja_retirada_em_casa", $_POST[id_loja_retirada_em_casa], "input-todas-bordas form-control fundo-form1 fundo-form cep-rodape") ?>
          </div>
          <div class="col-xs-4 pt0">
          <input type="submit" name="btn_retirada_em_casa" value="Continuar" class="btn btn-primary pull-right">
          </div>
        </div>
      </form>
    </div>

  </div>

</div>


          <div class="clearfix"></div>


        </div>
        <div class="modal-footer">
          <?php /* ?>
          <button type="button" class="close right15 pull-left" data-dismiss="modal" aria-label="Fechar">
          <span class="btn btn-danger" aria-hidden="true">&times; Fechar</span>
          </button>
          <?php */ ?>

          <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn-default">Cancelar</a>

        </div>
      </div>
    </div>
  </div>





  <?php
  //  verifico se ja foi dogitado o cep
  function verifica_cep_digitado(){
    ?>





    <script type="text/javascript">
    $(document).ready(function(){
      $('#myModalcep').modal({
        backdrop: 'static',
        keyboard: false  // to prevent closing with Esc button (if you want this too)
      })
    });
    </script>


    <?php
  }




  
//  verifico se e para armazenar o cep na session
if (isset($_POST[session_cep_entrega])) {

  $frete = $obj_carrinho->get_endereco_frete($_POST[session_cep_entrega]);

  if ($frete == false) {
    unset($_SESSION[entrega]);
    verifica_cep_digitado();
    Util::alert_bootstrap("<h1>O cep digitado não foi encontrado.</h1>");
  }else{
    if($frete[efetua_entrega] == 'on'){
      $_SESSION[entrega] = $frete;
      $_SESSION[entrega][retirada_loja] = 'NAO';
    } else{
      verifica_cep_digitado();
      Util::alert_bootstrap("<h1>Infelizmente não efetuamos entrega em sua região.</h1>");
    }
  }
}



//  verifico se e para efetuar a retirada na loja
if (isset($_POST[btn_retirada_em_casa])) {

  //  busco os dados da loja
  $dados = $obj_site->select_unico("tb_lojas", "idloja", $_POST[id_loja_retirada_em_casa]);
  $dados_endereco = $obj_carrinho->busca_endereco($dados[cep]);

  $_SESSION[entrega][origin_addresses] = $dados[endereco];
  $_SESSION[entrega][destination_addresses] = utf8_encode("Retirada na Loja - " . $dados[endereco]);
  $_SESSION[entrega][distance] = 0;
  $_SESSION[entrega][cep] = $dados[cep];
  $_SESSION[entrega][endereco] = Util::trata_dados_formulario( utf8_encode( "Retirada na Loja - " . $dados[titulo]));
  $_SESSION[entrega][bairro] = $dados_endereco[bairro];
  $_SESSION[entrega][valor_frete] = 0;
  $_SESSION[entrega][id_loja] = $dados[idloja];
  $_SESSION[entrega][efetua_entrega] = 'on';
  $_SESSION[entrega][retirada_loja] = 'SIM';
  $_SESSION[entrega][loja_nome] = $dados[titulo];
  
}


  //  verifico se o cep ja foi digitado
  if (!isset($_SESSION[entrega])) {
    verifica_cep_digitado();
  }


  //  VERIFICA SE A LOJA ESTA ABERTA
  if (isset($_SESSION[entrega])) {
    $lojas = $obj_site->verifica_loja_aberta($_SESSION[entrega][id_loja]);

    if($lojas[result] == 0){
      Util::alert_bootstrap( "A loja que atende sua localidade ainda encontra-se fechada.", "Aviso", false, array('href'=>'../cardapios', 'titulo'=>'Voltar ao cardapio', 'class'=>'btn btn-primary') );
    }

  }


  ?>





  <?php
  //  verifico se e para finalizar a venda
  if(isset($_POST[nome])) {

    //  verifico se os tamanhos foram selecionados
    foreach ($_SESSION[produto] as $key => $tamanho) {
      if ($tamanho[tamanho] == '') {
        $erro = true;
        break;
      }
    }

    if (!$erro) {
      if($obj_carrinho->finaliza_venda($_POST)){
        Util::alert_bootstrap("<h1>Seu pedido foi enviado com sucesso. Um email foi enviado com mais detalhes.<h1>", 'Aviso', false, array('href'=>'../cardapios', 'titulo'=>'Continuar', 'class'=>'btn btn-primary') );
      }else{
        Util::alert_bootstrap("<h1>Houve um erro ao efetuar seu pedido, por favor tente novamente.<h1>");
      }
    }else{
      Util::alert_bootstrap("<h1>Por favor, selecione todos os tamanhos dos produtos.<h1>");
    }

  }




  ?>

  <!-- ======================================================================= -->
  <!--  modal cep -->
  <!-- ======================================================================= -->







  <div class="row lista-pedido top90">

    <form action="<?php echo Util::caminho_projeto() ?>/mobile/pedido/" class="top20 bottom29 FormDadosPedido1" method="post" id="form_rodape">
      <!-- itens do pedido -->
      <div class="container formulario-fale">
        <div class="row">




          <div class="col-12 top50 lista-pedido" id="resultado">
            <h4>
              RESUMO DO PEDIDO
              <br><br>
              <span class="top5">Você está sendo atendido pela loja: <?php Util::imprime( Util::troca_value_nome($_SESSION[entrega][id_loja], 'tb_lojas', 'idloja', 'titulo') ); ?></span>
            </h4>

            <!-- descricao itens -->
            <div class="top25 form-pedido" id="">
              <!-- ======================================================================= -->
              <!-- LISTA OS ITENS DO CARRINHO  -->
              <!-- ======================================================================= -->
              <?php if(count($_SESSION[pedido]) > 0): ?>
                <div class="table-responsive">
                  <table class="table table-responsive">

                    <thead class="thead-default">
                      <tr>
                        <th>IMAGEM</th>
                        <th>DESCRIÇÃO</th>
                        <th class="text-center">DETALHES</th>
                        <th class="text-center">EXCLUIR</th>
                      </tr>
                    </thead>


                    <tbody>

                      <?php
                      foreach ($_SESSION[pedido] as $key => $produto):
                        $row = $obj_site->select_unico("tb_produtos", "idproduto", $key);
                        ?>
                        <tr>
                          <th scope="row"><img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>"  width="70" alt=""></th>
                          <td><h2><?php Util::imprime($row[titulo]); ?></h2></td>

                          <td class="text-center">

                            <!-- Button trigger modal -->
                            <a class="btn btn-laranja1" data-toggle="modal" data-target="#myModal-<?php echo $key ?>">
                              <i class="fa fa-bars"></i>
                            </a>

                            <!-- Modal -->
                            <div class="modal fade" id="myModal-<?php echo $key ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">DETALHES DO MEU PEDIDO</h4>
                                  </div>


                                  <div class="modal-body modal-pedido">

                                    <!-- ======================================================================= -->
                                    <!-- busca os precos  -->
                                    <!-- ======================================================================= -->
                                    <?php
                                    $idpreco = $_SESSION[pedido][$key][preco];
                                    $preco = $obj_site->select_unico("tb_produtos_precos", "idprodutopreco", $idpreco);
                                    $valor_total_agrupamento = ($preco[valor] * $_SESSION[pedido][$key][quantidade]) + ($preco[valor_embalagem]  * $_SESSION[pedido][$key][quantidade] );
                                    $valor_total_pedido += $preco[valor] * $_SESSION[pedido][$key][quantidade] + ($preco[valor_embalagem]  * $_SESSION[pedido][$key][quantidade] );
                                    ?>

                                    <div class="clearfix"></div>
                                    <div class="pedido-linha">
                                      <div class="pull-left"><p class="top10">QUANTIDADE</p></div>
                                      <div class="pull-right"><p class="top10"><?php echo $_SESSION[pedido][$key][quantidade] ?></p></div>
                                    </div>


                                    <div class="pedido-linha">
                                      <div class="pull-left"><p class="top10">TAMANHO</p></div>
                                      <div class="pull-right"><p class="top10"><?php Util::imprime($preco[titulo]); ?></p></div>
                                    </div>





                                    <!-- ======================================================================= -->
                                    <!-- busca os precos  -->
                                    <!-- ======================================================================= -->





                                    <?php
                                    foreach ($_SESSION[pedido][$key][item_agrupamento] as $key_agrupamento => $item_agrupamento):




                                      $result = $obj_site->select("tb_agrupamentos", "AND idagrupamento =  '$key_agrupamento' ");
                                      if (mysql_num_rows($result) > 0) {
                                        while ($row = mysql_fetch_array($result)) {

                                          ?>
                                          <div class="clearfix"></div>

                                          <h1 class="top10"><?php Util::imprime($row[titulo_abreviado]); ?></h1>
                                          <?php


                                          //  BUSCO OS ITENS SELECIONADO PELO USUARIO
                                          $valor_agrupamento = 0;
                                          foreach ($item_agrupamento as $keyiditemagrupamento => $iditemagrupamento) {

                                            $result1 = $obj_site->select("tb_itens_agrupamentos", "and iditemagrupamento = '$iditemagrupamento'");
                                            if (mysql_num_rows($result1) > 0) {
                                              echo '<div class="pedido-itens-agrupamento">';
                                              while ($row1 = mysql_fetch_array($result1)) {
                                                ?>
                                                <div class="clearfix"></div>
                                                <div class="pedido-linha">
                                                  <div class="pull-left"><p><?php Util::imprime($row1[titulo]); ?></p></div>

                                                </div>
                                                <?php
                                                $valor_agrupamento += $row1[valor] * $_SESSION[pedido][$key][quantidade];
                                                $valor_total_agrupamento += $row1[valor] * $_SESSION[pedido][$key][quantidade];
                                                $valor_total_pedido += $row1[valor] * $_SESSION[pedido][$key][quantidade];
                                              }
                                            }

                                          }

                                          ?>

                                        </div>
                                        <?php

                                        //  BUSCO OS ITENS SELECIONADO PELO USUARIO




                                      }
                                    }

                                  endforeach;
                                  ?>






                                  <div class="top10">
                                    <h1>OBSERVAÇÃO</h1>
                                    <p><?php echo($_SESSION[pedido][$key][mensagem]); ?> </p>
                                  </div>



                                  <br>
                                </div>


                                <div class="modal-footer">
                                  <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </td>
                        <td align="center">
                          <a href="?action=excluir&id=<?php echo $_SESSION[pedido][$key][idproduto] ?>">
                            <i class="fa fa-trash"></i>
                          </a>
                        </td>


                      </tr>
                      <?php
                    endforeach;
                    ?>

                    <tr>
                      <td colspan="2" align="right"><h2>TAXA DE ENTREGA</h2></td>
                      <td align="right"><h2>R$ <?php echo Util::formata_moeda($_SESSION[entrega][valor_frete]) ?></h2></td>
                      <td></td>
                    </tr>



                  </tbody>
                </table>
              </div>
            <?php endif; ?>
            <!-- ======================================================================= -->
            <!-- LISTA OS ITENS DO CARRINHO  -->
            <!-- ======================================================================= -->
          </div>
          <!-- descricao itens -->
        </div>



        <div class="col-12 bottom60">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" class="btn btn-laranja top20">
            CONTINUAR COMPRANDO
          </a>
        </div>


      </div>
    </div>
    <!-- itens do pedido -->
    <!-- ======================================================================= -->
    <!-- DADOS DA ENTREGA  -->
    <!-- ======================================================================= -->
    <div class="container">

      <div class="col-12 descricao-itens top50">
        <h4>DADOS DE ENTREGA</h4>
        <?php $dados = $obj_site->select_unico("tb_empresa", "idempresa", 6); ?>
        <p><?php Util::imprime($dados[descricao]) ?></p>
      </div>
      <!-- botao calcular -->
      <div class="">

        <div class="col-12 top15">
          <div class="form-group">
            
          <?php if($_SESSION[entrega][retirada_loja] == 'NAO'): ?>
              <h2>Cep atual: </h2>
              <h4 class="pull-left top10"><?php echo $_SESSION[entrega][cep] ?></h4>
            <?php else: ?>
               <h2>Loja para Retirada: </h2>
               <h4 class="pull-left top10"><?php Util::imprime( $_SESSION[entrega][loja_nome]) ?></h4>
            <?php endif; ?>

                    

            <a href="javascript:void(0);" class="top5 btn btn-laranja1 left10 troca-cep">Alterar</a>

          </div>
        </div>

        <div class="col-12 form-group">
          <h2>NOME</h2>
          <input type="text" name="nome" id="nome" class="form-control fundo-form1 fundo-form cep-rodape input100"  value="<?php Util::imprime($_SESSION[dados_entrega][nome]); ?>"  >
        </div>

        <div class="col-12 form-group">
          <h2>EMAIL</h2>
          <input type="text" name="email" id="email" class="form-control fundo-form1 fundo-form cep-rodape input100"   value="<?php Util::imprime($_SESSION[dados_entrega][email]); ?>"   >
        </div>

        <div class=" col-6 form-group">
          <h2>TEL. CELULAR</h2>
          <input type="text" name="celular" id="celular" class="form-control fundo-form1 fundo-form cep-rodape input100"    value="<?php Util::imprime($_SESSION[dados_entrega][celular]); ?>"   >
        </div>

        <div class="col-6 form-group">
          <h2>TEL. FIXO</h2>
          <input type="text" name="fixo" id="fixo" class="form-control fundo-form1 fundo-form cep-rodape input100"     value="<?php Util::imprime($_SESSION[dados_entrega][fixo]); ?>"  >
        </div>

        <div class="col-12 form-group">
          <h2>ENDEREÇO</h2>
          <input <?php if($_SESSION[entrega][retirada_loja] == 'SIM'): echo 'disabled'; endif; ?> type="text" name="endereco" id="endereco" class="form-control fundo-form1 fundo-form cep-rodape input100" value="<?php Util::imprime($_SESSION[entrega][endereco]) ?>"     >
        </div>
        

        <?php if($_SESSION[entrega][retirada_loja] == 'NAO'): ?>
          <div class="col-4 form-group">
            <h2>NÚMERO</h2>
            <input type="text" name="numero" id="numero" class="form-control fundo-form1 fundo-form cep-rodape input100"   value="<?php Util::imprime($_SESSION[dados_entrega][numero]); ?>"   >
          </div>

          <div class="col-8 form-group">
            <h2>COMPLEMENTO</h2>
            <input type="text" name="complemento" id="complemento" class="form-control fundo-form1 fundo-form cep-rodape input100"   value="<?php Util::imprime($_SESSION[dados_entrega][complemento]); ?>"   >
          </div>


          <div class="col-12 form-group">
            <h2>REFERÊNCIA</h2>
            <input type="text" name="referencia" id="referencia" class="form-control fundo-form1 fundo-form cep-rodape input100"   value="<?php Util::imprime($_SESSION[dados_entrega][referencia]); ?>"  >
          </div>
        <?php endif; ?>

        <div class="col-12 form-group">
          <h2>BAIRRO / CIDADE / UF</h2>
          <input type="text" name="bairro" id="bairro" class="form-control fundo-form1 fundo-form cep-rodape input100" value="<?php echo ($_SESSION[entrega][destination_addresses]); ?>" disabled    >
        </div>



      </div>
      <!-- botao calcular -->




            <!-- forma de pagamento -->
            <div class="col-xs-12 descricao-itens1">
              <h4>OPÇÃO DE RETORNO</h4>
              <div class="top5">

                <div class="col-xs-12">
                  <div class="radio">
                    <label>
                      <input type="radio" class="forma-pagamento" name="retorno" value="TELEFONE"  >
                      <h1><i class="fa fa-phone"></i>TELEFONE</h1>
                    </label>
                  </div>
                </div>


                <div class="col-xs-12">
                  <div class="radio">
                    <label>
                      <input type="radio" class="forma-pagamento" name="retorno" value="WHATZAP"  >
                      <h1><i class="fa fa-whatsapp"></i>WHATZAP</h1>
                    </label>
                  </div>
                </div>

                <div class="col-xs-12">
                  <div class="radio">
                    <label>
                      <input type="radio" class="forma-pagamento" name="retorno" value="EMAIL"  >
                      <h1><i class="fa fa-envelope"></i>EMAIL</h1>
                    </label>
                  </div>
                </div>



              </div>
            </div>
            <!-- forma de pagamento -->


      <!-- forma de pagamento -->
      <div class="col-12 descricao-itens1">
        <h4>FORMA DE PAGAMENTO</h4>
        <div class="top40">


          <div class="col-12">
            <div class="radio">
              <label>
                <input type="radio" class="forma-pagamento" name="forma_pagamento" value="Cartão Visa"  >
                <h1><i class="fa fa-credit-card fa-2x"></i>CARTÃO VISA</h1>
              </label>
            </div>
          </div>


          <div class="col-12">
            <div class="radio">
              <label>
                <input type="radio" class="forma-pagamento" name="forma_pagamento" value="Cartão Mastercard"  >
                <h1><i class="fa fa-credit-card fa-2x"></i>CARTÃO MASTERCAD</h1>
              </label>
            </div>
          </div>

          <div class="col-12">
            <div class="radio">
              <label>
                <input type="radio" class="forma-pagamento" name="forma_pagamento" value="Cartão Elo"  >
                <h1><i class="fa fa-credit-card fa-2x"></i>CARTÃO ELO</h1>
              </label>
            </div>
          </div>


          <div class="col-12">
            <div class="radio">
              <label>
                <input type="radio" class="forma-pagamento" name="forma_pagamento" value="dinheiro" >
                <h1><i class="fa fa-money fa-2x"></i>DINHEIRO</h1>
              </label>
            </div>

            <div class="troco-para">
              <h2>TROCO PARA</h2>
              <input type="text" name="troco" class="form-control fundo-form1 fundo-form cep-rodape input100 troco"  >
            </div>

          </div>




        </div>
      </div>
      <!-- forma de pagamento -->

      <!-- data de entrega -->
      <div class="col-12 descricao-itens1 bottom80">

        <?php if(isset($_SESSION[entrega])): ?>
          <!-- botao enviar -->
          <div class="col-12 bottom50 text-center">
            <input type="submit" value="FINALIZAR PEDIDO" class="btn btn-laranja">
          </div>
          <!-- botao enviar -->
        <?php else: ?>
          <a href="javascript:void(0);" class="top5 btn btn-laranja1 left15 troca-cep">Informar o cep de entrega</a>
        <?php endif; ?>

      </div>

      <!-- data de entrega -->
    </div>
    <!-- ======================================================================= -->
    <!-- DADOS DA ENTREGA  -->
    <!-- ======================================================================= -->


  </form>

</div>

</div>






<?php require_once("../includes/rodape.php") ?>

</body>



</html>










<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu nome'

          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'insira seu email'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Informe um telefone'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      localidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem1: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>



<script>
$(document).ready(function() {
  $('.FormCep').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      session_cep_entrega: {
        validators: {
          notEmpty: {

          },
          stringLength: {
            enabled: true,
            min: 8,
            max: 8,
            message: 'O CEP deve conter 8 dígitos númericos.'
          },
        }
      }
    }
  });
});
</script>




<script>
$(document).ready(function() {
  $('.FormRetiradaLoja').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      id_loja_retirada_em_casa: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>








<script>
$(document).ready(function() {
  $('.FormDadosPedido1').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      celular: {
        validators: {
          notEmpty: {

          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
      <?php if($_SESSION[entrega][retirada_loja] == 'NAO'): ?>
      numero: {
        validators: {
          notEmpty: {

          }
        }
      },
    <?php endif; ?>




    }

  });
});
</script>
