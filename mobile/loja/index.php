
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// INTERNA
$url =$_GET[get1];

if(!empty($url)){
	$complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_lojas", $complemento);

if(mysql_num_rows($result)==0){
	Util::script_location(Util::caminho_projeto()."/mobile/lojas/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

//  verifica se a loja está aberta
$lojas = $obj_site->verifica_loja_aberta($dados_dentro[idloja]);
?>

<!doctype html>
<html amp lang="pt-br">
<head>
	<?php require_once("../includes/head.php"); ?>
	<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>


	<style amp-custom>
	<?php require_once("../css/geral.css"); ?>
	<?php require_once("../css/topo_rodape.css"); ?>
	<?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



	<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 22); ?>
	.bg-interna{
		background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
	}
	</style>



	<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	<script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>

</head>





<body class="bg-interna">


	<?php
	$voltar_para = 'lojas'; // link de volta, exemplo produtos, dicas, servicos etc
	require_once("../includes/topo.php")
	?>


	<div class="row">
		<!-- ======================================================================= -->
		<!--  titulo geral -->
		<!-- ======================================================================= -->
		<div class="col-12 localizacao-pagina titulo_emp text-center">
			<amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_personalizada.png" height="15" width="214" alt=""></amp-img>
			<div class="">
				<h2>CONHEÇA A </h2>
				<h1 class="text-capitalize" ><?php Util::imprime($dados_dentro[titulo]); ?></h1>
			</div>
			<amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_personalizada.png" height="15" width="214" alt=""></amp-img>
		</div>
		<!-- ======================================================================= -->
		<!--  titulo -->
		<!-- ======================================================================= -->
	</div>




	<div class="row">
		<!-- ======================================================================= -->
		<!-- contatos lojas -->
		<!-- ======================================================================= -->
		<div class="col-12 loja ">
			<!-- ======================================================================= -->
			<!-- telefones  -->
			<!-- ======================================================================= -->
			<div class="media top35">
				<div class="media-left media-middle">
					<amp-img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_telefone_loja.png" height="24" width="24" alt=""></amp-img>
				</div>
				<div class="media-body">
					<div class="pull-left">
						<h3 class="media-heading top10">
							<?php Util::imprime($dados_dentro[ddd1]); ?> <?php Util::imprime($dados_dentro[telefone1]); ?>
						</h3>
					</div>
					<div class="pull-right">
						<a class="btn btn_telefone" href="tel:+55<?php Util::imprime($dados_dentro[ddd1]); ?> <?php Util::imprime($dados_dentro[telefone1]); ?>">
							CHAMAR
						</a>
					</div>

				</div>
			</div>

			<?php if (!empty($dados_dentro[telefone2])): ?>
				<div class="media top35">
					<div class="media-left media-middle">
						<i class="fa fa-whatsapp"></i>
					</div>
					<div class="media-body">
						<div class="pull-left">
							<h3 class="media-heading">
								<?php Util::imprime($dados_dentro[ddd2]); ?> <?php Util::imprime($dados_dentro[telefone2]); ?>
							</h3>
						</div>
						<div class="pull-right">
							<a class="btn btn_telefone" href="tel:+55<?php Util::imprime($dados_dentro[ddd2]); ?> <?php Util::imprime($dados_dentro[telefone2]); ?>">
								CHAMAR
							</a>
						</div>

					</div>
				</div>
			<?php endif; ?>
			<!-- ======================================================================= -->
			<!-- telefones  -->
			<!-- ======================================================================= -->



			<!-- ======================================================================= -->
			<!-- Horario -->
			<!-- ======================================================================= -->
			<div class="media top20">
				<div class="media-left media-middle">
					<amp-img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_agenda_loja.png" height="26" width="30" alt=""></amp-img>
				</div>
				<div class="media-body">
					<h3 class="media-heading">Horario de Funcionamento <br>
						<span><?php Util::imprime($lojas[horario_abertura], 5); ?> às <?php Util::imprime($lojas[horario_fechamento], 5); ?>
						</span>
					</h3>
				</div>
			</div>


			<!-- ======================================================================= -->
			<!-- horario  -->
			<!-- ======================================================================= -->

			<!-- ======================================================================= -->
			<!-- LOCALIZACAO  -->
			<!-- ======================================================================= -->
			<div class="media horario top20">
				<div class="media-left media-middle">
					<amp-img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_localizacao01.png" height="20" width="16" alt=""></amp-img>
				</div>
				<div class="media-body">
					<h3 class="media-heading">
						<span><?php Util::imprime($dados_dentro[endereco]); ?>
						</span>
					</h3>
				</div>
			</div>
			<!-- ======================================================================= -->
			<!-- LOCALIZACAO  -->
			<!-- ======================================================================= -->

			<div class="clearfix">  </div>
			<div class="top20">  </div>
			<a class="btn_fale_loja " href="<?php echo Util::caminho_projeto(); ?>/fale-conosco">
				<amp-img  src="<?php echo Util::caminho_projeto(); ?>/imgs/tire_duvidas.png" height="55" width="290" alt=""></amp-img>
			</a>


		</div>
	</div>




	<div class="row">
		<div class="col-12 top10 clientes_emp bottom30">


			<amp-carousel id="carousel-with-preview"
			width="450"
			height="314"
			layout="responsive"
			type="slides"
			autoplay
			delay="4000"
			>

			<?php
			$i = 0;
			$result = $obj_site->select("tb_galerias_lojas", "AND id_loja = '$dados_dentro[0]' ");
			if (mysql_num_rows($result) > 0) {
				while($row = mysql_fetch_array($result)){
					?>


					<amp-img
					src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
					layout="responsive"
					width="450"
					height="314"
					alt="<?php echo Util::imprime($row[titulo]) ?>">
				</amp-img>
				<?php
			}
			$i++;
		}
		?>
	</amp-carousel>



	<div class="carousel-preview text-center">

		<?php
		$i = 0;
		$result = $obj_site->select("tb_galerias_lojas", "AND id_loja = '$dados_dentro[0]' ");
		if (mysql_num_rows($result) > 0) {
			while($row = mysql_fetch_array($result)){
				?>

				<button on="tap:carousel-with-preview.goToSlide(index=<?php echo $i++; ?>)">

					<amp-img
					src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/bolinhas.png"
					width="21"
					height="21"
					layout="responsive"
					class="img-circle"
					alt="<?php echo Util::imprime($row[titulo]) ?>">
				</amp-img>
			</button>

			<?php
		}
		$i++;
	}
	?>

</div>



</div>
</div>


<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->
<div class="row">
	<div class="col-12 top30">
			<div class="text-center mapa">
				<h1>COMO CHEGAR</h1>
				<amp-img src="<?php echo Util::caminho_projeto(); ?>//mobile/imgs/barra_empresa_home.jpg" height="3" width="100" alt=""></amp-img>
			</div>
	</div>
</div>

<div class="row bottom50">
	<amp-iframe
	width="600"
	height="445"
	layout="responsive"
	sandbox="allow-scripts allow-same-origin allow-popups"
	frameborder="0"
	src="<?php Util::imprime($dados_dentro[src_place]); ?>">
</amp-iframe>
</div>

<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->

<?php require_once("../includes/rodape.php") ?>

</body>



</html>
